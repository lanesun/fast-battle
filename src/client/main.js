"use strict";

;(async function () {


require('script/main');

/***********************************************************************
 *
 *   REQUIRES
 *
 */
const object = await require("lib/object");
const eventer = await require("lib/eventer");
const array = await require("lib/array");
const collision = await require("lib/collision");
const IMGS = await require("script/IMGS");
const s2painter = await require("script/s2painter");
const painter = await require("script/painter");
const CONST = await require("var/const");
const app = await require("var/app");


/***********************************************************************
 *
 *   CONST VALUE
 *
 */
// Graphics
const EFFECT_LEVEL = 4;
const EFFECT_SHADOW = true;
const DO_FADE = true;
// Debug
const CHECK_FPS = true;
const SHOW_POSITION = true;
// Others
const BULLET_SIZE = 4;
const IMG_SWIDTH = 32;
const IMG_SHEIGHT = 32;
const IMG_SCALE = 4;
const WEAPON_SCALE = 1;
const SHADOW_ALPHA = 0.2;
const SHADOW_WIDTH = 40;
const DAMP_DEGREE = 0.0001;
const COMPANION_AI_INTERVAL = 5000;
const BLOCK_SIZE = 48;
const COLOR = {
    bullet: "#ffffff",
    hp_line_bg: "#ffffff",
    hp_line_healthy: "#73d216",
    hp_line_half_dying: "#edd400",
    hp_line_dying: "#ce5c00",
};


/***********************************************************************
 *
 *   MAP CONFIG
 *
 */
let Map = {
    startx: -1600,
    starty: -1600,
    width: 3200,
    height: 3200
}


/***********************************************************************
 *
 * MATH NUMBERS
 *
 */
const NU_1S2 = 1/(2**(0.5));
const NU_S2 = 2**(0.5);


/***********************************************************************
 *
 *   TOOLS LIBRARY
 *
 */
const transX = (x,y) => x * NU_1S2 + y;
const transY = (x,y) => y - x * NU_1S2;
const transScreenX = (x, y) => transX(x - canvas.width/2, y - canvas.height/2) + s2painter.position.x;
const transScreenY = (x, y) => transY(x - canvas.width/2, y - canvas.height/2) + s2painter.position.y;
const getDistance = (a, b) => getDistance2(a,b)**0.5;
const getDistance2 = (a, b) => (a.x - b.x)**2 + (a.y - b.y)**2;
const getAngle = (agl) => (agl < 0 ? agl + Math.PI * 2 : agl >= Math.PI * 2 ? agl - Math.PI * 2 : agl);
const inBetween = (a, b, c) => (b > c ? c : b < a ? a : b);
const smallerThan = (a, b) => (a > b ? b : a);
const largerThan = (a, b) => (a < b ? b : a);
const rollBetween = (a, b, c) => (b > c ? b - c + a : b < a ? b - a + c : b);
const closeTo = (a, b, n) => (a > b ? largerThan(a - n, b) : smallerThan(a + n, b));
const absPlus = (a, b) => closeTo(a, 0, -n);


/***********************************************************************
 *
 *   VARS
 *
 */
let logs = [];
let units = [];
let bullets = [];
let effects = [];
let back_effects = [];
let active = true;
let TEAMS = ['a','b','c'];
let COMPANIONS = {
    a: new Set(["a","b"]),
    b: new Set(["b","a"]),
    c: new Set(["c"])
};


/***********************************************************************
 *
 *   UNITS
 *
 */
/*
const UNITS = {
    Unit: object.create({
        name: "Unit",
        extends: [eventer.EventTarget],
        constructor: function () {
            this.super0();
        },
        proto: {
            visible: false,
            collisible: false,
            thinking: false,
            logical: false,
            delete: (self) => {
                array.delete(units, self);
            },
        }
    })
};

let FEATURES = {};
FEATURES.Move = object.create({
    name: "Move",
    constructor: function (x,y) {
        this.x = x;
        this.y = y;
        this.vx = 0;
        this.vy = 0;
        this.ax = 0;
        this.ay = 0;
        this.vself = 0;
        this.face = 0;
    },
    proto: {
        logical: true,
        max_speed: 0,
        max_acceleration: 0,
        damp: 0,
        logicMove: (self, t) => {
            self.x += self.vx * t + Math.cos(self.face) * self.vself * t;
            self.y += self.vy * t + Math.sin(self.face) * self.vself * t;
            self.vx = closeTo(self.vx, 0, t * self.damp);
            self.vy = closeTo(self.vy, 0, t * self.damp);
        },
        logicH: (self, t) => {
            self.logicMove(self, t);
        }
    }
});
FEATURES.Visible = object.create({
    name: "Visible",
    constructor: function () {},
    proto: {
        visible: true,
        drawH: null
    }
});
FEATURES.Hp = object.create({
    name: "Hp",
    constructor: function () {
        this.hp = this.max_hp;
    },
    proto: {
        max_hp: 0,
        getHurt: (self, hp) => {
            self.hp -= hp;
            if (self.hp < 0) {
                self.delete();
            }
        }
    }
});
FEATURES.ArmoredHp = object.create({
    name: "ArmoredHp",
    extend: FEATURES.Hp,
    constructor: function () {
        this.super();
    },
    proto: {
        armor: 1,
        getArmoredHurt: (self, hp, strength) => {
            self.getHurt(strength / self.armor / (1 + Math.abs(strength - self.armor)) * hp);
        }
    }
});
FEATURES.Armed = object.create({
    name: "Armed",
    constructor: function () {
        this.power = this.max_power;
        this.weapon = null;
    },
    proto: {
        max_power: 0,
        logical: true,
        CMD_fire: (self, ...targets) => {
            return self.weapon.fire(self, ...targets);
        },
        logicPower: (self, t) => {
            self.power = smallerThan(self.power + t, self.max_power);
        },
        logicH: (self, t) => {
            self.logicPower(self, t);
        }
    }
});
FEATURES.SimpleMove = object.create({
    name: "SimpleMove",
    extend: FEATURES.Move,
    constructor: function () {
        this.super();
        this.target_speed = 0;
    },
    proto: {
        CMD_changeFace: (self, face) => {
            self.face = getAngle(face);
        },
        CMD_speedTo: (self, speed) => {
            self.target_speed = inBetween(-self.max_speed, speed, self.max_speed);
        },
        logicSimpleMove: (self, t) => {
            self.vself = closeTo(self.vself, self.target_speed, t * self.max_acceleration);
        },
        logicH: (self, t) => {
            self.logicSimpleMove(self,t);
        }
    }
});
FEATURES.SimpleVisible = object.create({
    name: "SimpleVisible",
    extend: FEATURES.Visible,
    constructor: function () {
        this.super();
        this.animation = 0;
        this.old_animation_status = 'stand';
        this.animation_time = 0;
    },
    proto: {
        animation_data: {
            stand: {
                timeout: 0,
                frames: [[],[]],
                length: 0
            },
            walk: {
                timeout: 0,
                frames: [[],[]],
                length: 0
            }
        },
        blood_frames: [
            await IMGS.get("res/gui.png",120,0,16,8,32,16),
            await IMGS.get("res/gui.png",120,8,16,8,32,16),
            await IMGS.get("res/gui.png",120,16,16,8,32,16),
            await IMGS.get("res/gui.png",120,24,16,8,32,16),
            await IMGS.get("res/gui.png",120,32,16,8,32,16)
        ],
        shadow_frame: await IMGS.get("res/shadow.png",0,0,32,23,40,28),
        getAnimationStatus: (self) => {
            if (self.vself !== 0) {
                return 'walk';
            }
            return 'stand';
        },
        getAnimationFrame: (self) => {
            if (rollBetween(-Math.PI, self.face + Math.PI/4*3, Math.PI) > 0) {
                return self.animation_data[self.getAnimationStatus(self)].frames[0][self.animation];
            } else {
                return self.animation_data[self.getAnimationStatus(self)].frames[1][self.animation];
            }
        },
        logicRefreshAnimationFrame: (self, t) => {
            let astatus = self.getAnimationStatus(self);
            if (astatus !== self.old_animation_status) {
                self.animation = 0;
                self.animation_time = 0;
                self.old_animation_status = astatus;
            } else {
                self.animation_time += t;
                if (self.animation_time >= self.animation_data[astatus].timeout) {
                    self.animation = rollBetween(-1, self.animation + 1, self.animation_data[astatus].length - 1);
                    self.animation_time = 0;
                }
            }
        },
        logicH: (self, t) => {
            self.logicRefreshAnimationFrame(self,t);
        },
        drawH: (self, ctx) => {
            let img = self.getAnimationFrame(self);
            ctx.drawImage(img, self.x, self.y);
            if (self.hp < self.max_hp) {
                ctx.drawImage(self.blood_frames[Math.round(self.hp/self.max_hp * 4)], self.x, self.y, 0, -img.h);
            }
        },
        lowerDrawH: (self, ctx) => {
            if (EFFECT_SHADOW) ctx.drawImage(self.shadow_frame, self.x, self.y, 0, self.shadow_frame.height/2 - 4);
        }
    }
});
FEATURES.RoundCollisible = object.create({
    name: "RoundCollisible",
    constructor: function () {},
    proto: {
        collisible: true,
        collision_size: 0,
        collision_type: 'round',
        collisionH: (self, target) => {
            switch (target.collision_type) {
                case 'round':
                    collision.rdrdCollision(self, target);
                    break;
                case 'rect':
                    collision.rdrtCollision(self, target);
                    break;
            }
        }
    }
});
FEATURES.RectCollisible = object.create({
    name: "RectCollisible",
    constructor: function () {},
    proto: {
        collisible: true,
        collision_width: 0,
        collision_height: 0,
        collision_type: 'rect',
        collisionH: (self, target) => {
            switch (target.collision_type) {
                case 'round':
                    collision.rdrtCollision(target, self);
                    break;
                case 'rect':
                    collision.rtrtCollision(self, target);
                    break;
            }
        }
    }
});

const MOUDlES = {
    Person: object.create({
        name: "Person",
        extend: UNITS.Unit,
        extends: [
            FEATURES.ArmoredHp,
            FEATURES.Armed,
            FEATURES.SimpleMove,
            FEATURES.SimpleVisible,
            FEATURES.RoundCollisible
        ],
        constructor: function (x,y) {
            this.x = x;
            this.y = y;
            this.vx = 0;
            this.vy = 0;
            this.ax = 0;
            this.ay = 0;
            this.vself = 0;
            this.face = 0;
            this.hp = this.max_hp;
            this.power = this.max_power;
            this.weapon = null;
            this.target_speed = 0;
            this.animation = 0;
            this.old_animation_status = 'walk';
            this.animation_time = 0;
        },
        proto: {
            max_speed: 0,
            max_acceleration: 0,
            damp: 0,
            max_hp: 0,
            armor: 1,
            max_power: 0,
            animation_data: {
                stand: {
                    timeout: 0,
                    frames: [[],[]],
                    length: 0
                },
                walk: {
                    timeout: 0,
                    frames: [[],[]],
                    length: 0
                }
            },
            collision_size: 0,
        }
    }),
    RectCollision: object.create({
        name: "RectCollision",
        extend: UNITS.Unit,
        extends: [
            FEATURES.RectCollisible
        ],
        constructor: function (x,y,w,h) {
            this.x = x;
            this.y = y;
            this.collision_width = w;
            this.collision_height = h;
        },
        proto: {
            locked: true,
            collision_width: 0,
            collision_height: 0,
            collisionH: null
        }
    })
};


const ROLE = {
    Gun_Soldier: object.create({
        name: "Gun_Soldier",
        extend: UNITS.Unit,
        extends: [
            FEATURES.ArmoredHp,
            FEATURES.Armed,
            FEATURES.SimpleMove,
            FEATURES.SimpleVisible,
            FEATURES.RoundCollisible
        ],
        constructor: function (x,y) {
            this.x = x;
            this.y = y;
            this.vx = 0;
            this.vy = 0;
            this.ax = 0;
            this.ay = 0;
            this.vself = 0;
            this.face = 0;
            this.hp = this.max_hp;
            this.power = this.max_power;
            //this.weapon = new WEAPON.Gun;
            this.target_speed = 0;
            this.animation = 0;
            this.old_animation_status = 'walk';
            this.animation_time = 0;
        },
        proto: {
            thinking: true,
            max_speed: 0.2,
            max_acceleration: 0.001,
            damp: 0.0001,
            max_hp: 100,
            armor: 1,
            max_power: 500,
            animation_data: {
                stand: {
                    timeout: 0,
                    frames: [[
                        await IMGS.get("res/soldier.png",0,0,32,32,80,80),
                    ],[
                        await IMGS.get("res/soldier.png",0,32,32,32,80,80),
                    ]],
                    length: 1
                },
                walk: {
                    timeout: 50,
                    frames: [[
                        await IMGS.get("res/soldier.png",0,0,32,32,80,80),
                        await IMGS.get("res/soldier.png",32,0,32,32,80,80),
                        await IMGS.get("res/soldier.png",64,0,32,32,80,80),
                        await IMGS.get("res/soldier.png",96,0,32,32,80,80),
                    ],[
                        await IMGS.get("res/soldier.png",0,32,32,32,80,80),
                        await IMGS.get("res/soldier.png",32,32,32,32,80,80),
                        await IMGS.get("res/soldier.png",64,32,32,32,80,80),
                        await IMGS.get("res/soldier.png",96,32,32,32,80,80),
                    ]],
                    length: 4
                }
            },
            collision_size: 25,
            logicH: (self, t) => {
                self.logicMove(self, t);
                self.logicPower(self, t);
                self.logicSimpleMove(self,t);
                self.logicRefreshAnimationFrame(self,t);
            },
            ai: (self, units) => {
                self.CMD_changeFace(self, Math.atan2(transScreenY(Mouse.x, Mouse.y) - self.y, transScreenX(Mouse.x, Mouse.y) - self.x));
                self.CMD_speedTo(self,self.max_speed);
            }
        }
    }),
};

function drawUnits () {
    units.sort((unit1,unit2) => (unit1.x + unit1.y  - unit2.x - unit2.y));
    for (let unit of units) {
        if (unit.visible) {
            unit.drawH(unit, s2painter);
        }
    }
}

function drawLowerUnits () {
    units.sort((unit1,unit2) => (unit1.x + unit1.y  - unit2.x - unit2.y));
    for (let unit of units) {
        if (unit.visible && unit.lowerDrawH) {
            unit.lowerDrawH(unit, s2painter);
        }
    }
}

const img_ground = await IMGS.get("res/iso-grass.png", 0, 0, 32, 32, 68, 68);

function drawGround () {
    for (let i = Map.startx; i < Map.width + Map.startx; i += BLOCK_SIZE) {
        for (let j = Map.starty; j < Map.height + Map.starty; j += BLOCK_SIZE) {
            s2painter.drawImage(img_ground, i, j);
        }
    }
}

function drawTick () {
    s2painter.clear();
    drawGround();
    drawLowerUnits();
    drawSelectBox();
    drawUnits();
    if (SHOW_POSITION) drawCenter();
    drawMouse();
}

function mainLoop () {
    if (active) {
        drawTick();
        calcTick();
        requestAnimationFrame(mainLoop);
    }
}

function startLoop () {
    active = true;
    mainLoop();
}

function stopLoop () {
    active = false;
}

if (SHOW_POSITION) {
    aconsole.printFn(() => {
        return "[Point at (X:" + parseInt(transScreenX(Mouse.x, Mouse.y)) + " Y:" + parseInt(transScreenY(Mouse.x, Mouse.y)) + ")]"
    },Infinity,"#eee");
    aconsole.printFn(() => {
        return "[Center at (X:" + parseInt(s2painter.position.x) + " Y:" + parseInt(s2painter.position.y) + ")]"
    },Infinity,"#eee");
}

function drawCenter () {
    let ctx = painter.context;
    ctx.lineWidth = 3;
    ctx.beginPath();
    ctx.moveTo(canvas.width/2,canvas.height/2 - 24);
    ctx.lineTo(canvas.width/2,canvas.height/2 + 24);
    ctx.moveTo(canvas.width/2 - 24,canvas.height/2);
    ctx.lineTo(canvas.width/2 + 24,canvas.height/2);
    ctx.strokeStyle = "#fff";
    ctx.stroke();
}

let old_date = null;
let fps = 0;
let fps_refresh_time = 0;
let fps_records = [];
if (CHECK_FPS) {
    aconsole.printFn(() => {
        //let bar = "";
        //for (let i = 0; i < fps; i += 3) {
            //bar += "#";
        //}
        return "[FPS:" + fps +  "]";
    },Infinity,"#ff0");
}

function fpsCheck (t) {
    fps_records.push(t);
    if (fps_records.length > 100) {
        fps_records.shift();
    }
    fps_refresh_time += t;
    if (fps_refresh_time > 20) {
        fps_refresh_time = 0;
        let t = 0;
        let rt = 0;
        for (let record of fps_records) {
            t += 1000;
            rt += record;
        }
        fps = 0|(t/rt);
    }
}    

function calcTick () {
    if (old_date) {
        let t = new Date() - old_date;
        old_date = new Date();
        if (t > 50) t = 50;//CONST
        if (CHECK_FPS) fpsCheck(t);
        aconsole.drawer(painter.context, t);
        calcSelectBoxAni(t);
        doLogic(t);
        doThink(t);
        doCollision();
    } else {
        old_date = new Date();
    }
}

function doLogic (t) {
    for (let unit of units) {
        if (unit.logical) {
            unit.logicH(unit, t);
        }
    }
}

function doThink (t) {
    for (let unit of units) {
        if (unit.thinking) {
            unit.ai(unit, units, t);
        }
    }
}

function doCollision () {
    let cunits = units.filter((unit) => unit.collisible);
    for (let unit of cunits.filter((unit) => unit.collisionH)) {
        for (let target of cunits) {
            if (target !== unit) {
                unit.collisionH(unit, target);
            }
        }
    }
}

let addUnitByPos = () => {
    let x = Mouse.x;
    let y = Mouse.y;
    if (!Mouse.lb) return;
    units.push(new ROLE.Gun_Soldier(transScreenX(x,y),transScreenY(x,y)));
    m--;
}

let m = 200;
//window.setInterval(() => {
    //addUnitByPos();
//}, 20);

let select_box_agl = 0;

function calcSelectBoxAni (t) {
    if (Mouse.selecting) {
        select_box_agl = select_box_agl > Math.PI * 2 ? 0 : select_box_agl + 0.01 * t;//CONST
    }
}

function drawSelectBox () {
    if (Mouse.selecting) {
        //TODO
    }
}

function drawMouse () {
    if (Mouse.inside) {
        let img = Mouse.rb ? Mouse.grab : Mouse.pointer;
        painter.drawImage(img, Mouse.x, Mouse.y);
    }
}

let Mouse = {
    x: 0,
    y: 0,
    rb: false,
    lb: false,
    inside: false,
    selecting: false,
    pointer: await IMGS.get("res/gui.png", 48, 0, 16, 16, 32, 32),
    grab: await IMGS.get("res/gui.png", 64, 0, 16, 16, 32, 32),
}

let SelectBox = {
    x1: 0,
    y1: 0,
    x2: 0,
    y2: 0
}

let mousedown = (e) => {
    Mouse.inside = true;
    if (e.button === 0) {
        Mouse.lb = true;
        Mouse.selecting = true;
        SelectBox.x1 = SelectBox.x2 = transScreenX(e.offsetX,e.offsetY);
        SelectBox.y1 = SelectBox.y2 = transScreenY(e.offsetX,e.offsetY);
        addUnitByPos();
    }
    if (e.button === 2) {
        Mouse.rb = true;
    }
}

let mouseup = (e) => {
    Mouse.inside = true;
    if (e.button === 0) {
        Mouse.lb = false;
        Mouse.selecting = false;
    }
    if (e.button === 2) {
        Mouse.rb = false;
    }
}

let mousemove = (e) => {
    Mouse.inside = true;
    moveView(e);
    Mouse.x = e.offsetX;
    Mouse.y = e.offsetY;
    if (Mouse.selecting) {
        SelectBox.x2 = transScreenX(e.offsetX,e.offsetY);
        SelectBox.y2 = transScreenY(e.offsetX,e.offsetY);
    }
}

let mouseleave = (e) => {
    Mouse.inside = false;
}

let moveView = (e) => {
    let x = e.movementX;
    let y = e.movementY;
    if (Mouse.rb) {
        s2painter.position.x -= transX(x, y);
        s2painter.position.y -= transY(x, y); 
    }
}

let last_command = "";

let help = () => {
    return `
欢迎使用控制台模块（使用"h"键关闭输出消隐）

在控制台中，你可以操控游戏的方方面面，
通过 "CONST.<参数名> = <参数值>" 指令，你可以修改游戏的静态设置参数
通过 "app.<参数名> = <参数值>" 指令，你可以修改游戏的运行时数据
`;
}

let keydown = (e) => {
    if (e.key === "/") {
        last_command = prompt("Command line", last_command);
        try {
            aconsole.print(eval(last_command));
        } catch (e) {
            aconsole.print(e.toString());
        }
    } else if (e.key === "h") {
        aconsole.fade = !aconsole.fade;
    }
}

canvas_gui.oncontextmenu = (e) => {e.preventDefault()};
canvas_gui.addEventListener("mousemove", mousemove);
window.addEventListener("mousedown", mousedown);
window.addEventListener("mouseup", mouseup);
window.addEventListener("mouseout", mouseleave);
window.addEventListener("keydown", keydown);
startLoop();
aconsole.print("------------------------------------------", 10000)
aconsole.print("         Fast Battle By Lane Sun          ", 10000);
aconsole.print("                  v1.1.1                  ", 10000, "#ff0");
aconsole.print("  Visit my Website to check out updates.  ", 10000);
aconsole.print("           https://anlbrain.com           ", 10000, "#09f");
aconsole.print("------------------------------------------", 10000)

units.push(new MOUDlES.RectCollision(Map.startx - 100, (Map.starty * 2 + Map.height)/2, 200, Map.height));
units.push(new MOUDlES.RectCollision(Map.startx + Map.width + 100, (Map.starty * 2 + Map.height)/2, 200, Map.height));
units.push(new MOUDlES.RectCollision((Map.startx * 2 + Map.width)/2, Map.starty - 100, Map.width, 200));
units.push(new MOUDlES.RectCollision((Map.startx * 2 + Map.width)/2, Map.starty + Map.height + 100, Map.width, 200));
*/
})();
