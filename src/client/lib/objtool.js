/*
~
~ 对象辅助库
~ 用于解决诸如交叉继承等问题
~
*/

;moudle = async function (objtool) {
    const random = await require('lib/random');
    let ids = new Set();
    let generate = () => {
        do {
            code = random.duuid();
        } while (ids.has(code));
        ide.add(code);
        return code;
    };
    objtool.onceCon = (fn, id = generate()) => {
        return function (...args) {
            if (!this._conmap) {
                this._conmap = new Set();
            }
            if (!this._conmap.has(id)) {
                this._conmap.add(id);
                fn.call(this, ...args);
            }
        }
    };
    objtool.onceLambda = (fn, id = generate()) => {
        return (self, ...args) => {
            if (!self._fnmap) {
                self._fnmap = new Set();
            }
            if (!self._fnmap.has(id)) {
                self._fnmap.add(id);
                fn(self, ...args);
            }
        }
    };
};
