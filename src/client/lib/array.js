/*
~
~ 数组对象的操作库
~
*/

;moudle = async function (array) {
    array.last = (ary) => {
        return ary[ary.length - 1];
    };
    array.any = (ary) => {
        return ary.length ? true : false;
    };
    array.sliceEnd = (ary, num) => {
        return ary.slice(0, ary.length - num - 1);
    };
    array.delete = (ary, elem) => {
        ary.splice(ary.indexOf(elem), 1);
    };
};
