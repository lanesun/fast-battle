/*
~
~ 碰撞函数库，目前只有矩形碰撞。。。（删去不实言论）
~ 但也够用了。。。
~
*/

;moudle = async function (collision) {
    let getDistance = (a, b) => ((a.x - b.x)**2 + (a.y - b.y)**2)**0.5;
    collision.against = (A, B) => {
        return !((A[2] < B[0] || A[0] > B[2]) || (A[3] < B[1] || A[1] > B[3]));
    };
    collision.beInFn = (A, B) => {
        let x = B.getBoxX(B), y = B.getBoxY(B), w = B.getBoxW(B), h = B.getBoxH(B);
        return Math.abs(A.x - x) - w/2 < 0 && Math.abs(A.y - y) - h/2 < 0 ? true : false;
    };
    collision.rdrdCollisionSoft = (A, B) => {//圆形对圆形碰撞
        let dis = A.collision_size + B.collision_size - getDistance(A, B);
        if (dis > 0) {
            let agl = Math.atan2(B.y - A.y, B.x - A.x);
            if (!B.locked) B.x += Math.cos(agl) * dis * A.collision_size / (A.collision_size + B.collision_size) / 6;
            if (!B.locked) B.y += Math.sin(agl) * dis * A.collision_size / (A.collision_size + B.collision_size) / 6;
            if (!A.locked) A.x -= Math.cos(agl) * dis * B.collision_size / (A.collision_size + B.collision_size) / 6;
            if (!A.locked) A.y -= Math.cos(agl) * dis * B.collision_size / (A.collision_size + B.collision_size) / 6;
        }
    };
    collision.rdrdCollision = (A, B) => {//圆形对圆形碰撞，只改变速度
        let dis = A.collision_size + B.collision_size - getDistance(A, B);
        if (dis > 0) {
            let agl = Math.atan2(B.y - A.y, B.x - A.x);
            if (!B.locked) B.x += Math.cos(agl) * dis * A.collision_size / (A.collision_size + B.collision_size) / 2;
            if (!B.locked) B.y += Math.sin(agl) * dis * A.collision_size / (A.collision_size + B.collision_size) / 2;
            if (!A.locked) A.x -= Math.cos(agl) * dis * B.collision_size / (A.collision_size + B.collision_size) / 2;
            if (!A.locked) A.y -= Math.cos(agl) * dis * B.collision_size / (A.collision_size + B.collision_size) / 2;
        }
    };
    collision.rdrtCollision = (A, B) => {//圆形对矩形碰撞
        let disX = (A.collision_size + B.collision_width)/2 - Math.abs(B.x - A.x);
        let disY = (A.collision_size + B.collision_height)/2 - Math.abs(B.y - A.y);
        disX = disX > 0 ? (A.x < B.x ? 1:-1) * disX : 0;
        disY = disY > 0 ? (A.y < B.y ? 1:-1) * disY : 0;
        if (disX === 0 || disY ===0) {
            return;
        } else if (Math.abs(disX) < Math.abs(disY)) {
            if (!B.locked) B.x += disX * (A.collision_size * 2) / (A.collision_size * 2 + B.collision_width + B.collision_height) / 2;
            if (!A.locked) A.x -= disX * (B.collision_width + B.collision_height) / (A.collision_size * 2 + B.collision_width + B.collision_height) / 2;
        } else {
            if (!B.locked) B.y += disY * (A.collision_size * 2) / (A.collision_size * 2 + B.collision_width + B.collision_height) / 2;
            if (!A.locked) A.y -= disY * (B.collision_width + B.collision_height) / (A.collision_size * 2 + B.collision_width + B.collision_height) / 2;
        }
    };
    collision.rtrtCollision = (A, B) => {//矩形对矩形碰撞
        let disX = (A.collision_width + B.collision_width)/2 - Math.abs(B.x - A.x);
        let disY = (A.collision_height + B.collision_height)/2 - Math.abs(B.y - A.y);
        disX = disX > 0 ? (A.x < B.x ? 1:-1) * disX : 0;
        disY = disY > 0 ? (A.y < B.y ? 1:-1) * disY : 0;
        if (disX === 0 || disY ===0) {
            return;
        } else if (Math.abs(disX) < Math.abs(disY)) {
            if (!B.locked) B.x += disX * (A.collision_width + A.collision_height) / (A.collision_width + A.collision_height + B.collision_width + B.collision_height) / 2;
            if (!A.locked) A.x -= disX * (B.collision_width + B.collision_height) / (A.collision_width + A.collision_height + B.collision_width + B.collision_height) / 2;
        } else {
            if (!B.locked) B.y += disY * (A.collision_width + A.collision_height) / (A.collision_width + A.collision_height + B.collision_width + B.collision_height) / 2;
            if (!A.locked) A.y -= disY * (B.collision_width + B.collision_height) / (A.collision_width + A.collision_height + B.collision_width + B.collision_height) / 2;
        }
    };
};
