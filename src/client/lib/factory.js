/*
~
~ 这个库"绝对"只有我会用。。。
~ 如果你真的想了解，建议看一下我博客站中关于aLib.js中codeunit的文章
~
*/

;moudle = function (factory) {
    factory.fromFunction = function (fn, data) {
        return (...args) => {
            let sets = [];
            for (let i = 0, j = 0; i < data.length; i++) {
                sets[i] = data[i] === null ? args[j++] : data[i];
            }
            return fn(...sets);
        };
    };
    factory.fromObject = function (fn, data) {
        return function (...args) {
            let sets = [];
            for (let i = 0, j = 0; i < data.length; i++) {
                sets[i] = data[i] === null ? args[j++] : data[i];
            }
            fn.call(this, ...sets);
        };
    };
};
