/*
~
~ 场景库
~ 第一次做这种库。。。
~ 已经严重超出了。。。但还有功能没添加
~ 游戏特定版
~
*/

;moudle = async function (scence) {
    const
    object = await require('lib/object'),
    factory = (await require('lib/factory')).fromFunction,
    app = await require('var/app'),
    FLAG = await require('var/flag'),
    CONST = await require('var/const');
    scence.Scence = object.create({
        method: object.METHOD_MERGE,
        name: "Scence",
        constructor: function (isactive = true, doclear = true) {
            this.animation_active = isactive;
            this.animation_tick = false;
            this.clear_active = doclear;
            this.clear_tick = false;
            this.canvas = null;
            this.context = null;
            this.scence_lists = new Map();
            this.scence_stack = [];
            this.promise_stack = [];
            this.drawer_stack = [];
            this.controller_stack = [];
            this.signed_events = new Map();
            const self = this;
            let _event_handle = (e, name) => {
                for (let h of self.controller_stack) {
                    if(h[name]) {
                        if (h[name](e) === false) break;
                    }
                }
            };
            this.createEventHandle = name => factory(_event_handle, [null, name]);
        },
        extend: object.EmptyObject,
        proto: {
            _signEvent: function (name) {
                if (!this.signed_events.has(name)) {
                    this.signed_events.set(name, null);
                }
            },
            _bindEvent: function (name) {
                this.signed_events.set(name, this.createEventHandle(name));
                if (CONST.GLOBAL_EVENTS.includes(name)) {
                    document.addEventListener(name, this.signed_events.get(name));
                } else {
                    this.canvas.addEventListener(name, this.signed_events.get(name));
                }
            },
            init: function (canvas) {
                let self = this;
                let old_date = null;
                let drawer = () => {
                    let new_date = new Date();
                    if (!old_date) {
                        old_date = new_date;
                        window.requestAnimationFrame(drawer);
                        return;
                    }
                    let t = new_date - old_date;
                    old_date = new_date;
                    if (t > CONST.MAX_FRAME_TIMEOUT) t = CONST.MAX_FRAME_TIMEOUT;
                    if (self.animation_active || self.animation_tick) {
                        self.animation_tick = false;
                        if (self.clear_active || self.clear_tick) {
                            self.clear_tick = false;
                            self.context.clearRect(0,0,self.canvas.width,self.canvas.height);//TODO 太长了。。。以后用canvas2d库包装一下
                        }
                        self.drawer_stack.forEach(d => {
                            d(self.context, t);
                        });
                    }
                    window.requestAnimationFrame(drawer);
                };
                this.canvas = canvas;
                this.context = canvas.getContext('2d');
                this.context.imageSmoothingEnabled = false;
                this.signed_events.forEach((value, name) => {
                    if (!value) {
                        this._bindEvent(name);
                    }
                }, this);
                drawer();
            },
            signScence: function (name, handle) {
                this.scence_lists.set(name, handle);
            },
            pushScence: function (name, ...arg) {
                let handle = null;
                let promise = new Promise(resolve => {  //TODO 这个resolve句柄的提取操作可以包装起来
                    handle = resolve;
                });
                this.promise_stack.unshift(handle);
                this.scence_stack.unshift(name);
                this.scence_lists.get(name)(...arg);
                return promise;
            },
            pushDrawer: function (drawer) {
                this.drawer_stack.push(drawer);
            },
            pushController: function (controller) {
                for (let name in controller) {
                    if (!this.signed_events.has(name)) {
                        if (this.canvas) {
                            this._bindEvent(name);
                        } else {
                            this._signEvent(name);
                        }
                    }
                }
                this.controller_stack.push(controller);
            },
            pushElem: function (elem) {
                document.body.appendChild(elem);
            },
            exitScence: function (...arg) {
                this.promise_stack.shift()(...arg);
                this.scence_stack.shift();
            },
            exitDrawer: function (drawer) {
                this.drawer_stack.splice(this.drawer_stack.indexOf(drawer), 1);
            },
            exitController: function (controller) {
                this.controller_stack.splice(this.controller_stack.indexOf(controller), 1);
            },
            exitElem: function (elem) {
                document.body.removeChild(elem);
            }
        }
    });
};
