/*
~
~ 一个可复用Eventer类
~
*/

;moudle = async function (eventer) {
    const object = await require("lib/object");
    eventer.EventTarget = object.create({
        method: object.METHOD_MERGE,
        name: "EventTarget",
        constructor: function () {
            this.events = new Map();
        },
        extend: object.EmptyObject,
        proto: {
            on: function (name, handle) {
                if (this.events.has(name)) {
                    this.events.get(name).push(handle);
                } else {
                    this.events.set(name, [handle]);
                }
            },
            trigger: function (name, ...args) {
                if (this.events.has(name)) {
                    this.events.get(name).forEach((handle) => {
                        handle.call(this, ...args);
                    });
                }
            }
        }
    });
};
