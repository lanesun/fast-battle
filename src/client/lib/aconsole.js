/*
~
~ 一个简单的输出面板
~
*/

;moudle = async function (aconsole) {
    const object = await require("lib/object");
    const calc = await require("lib/calc");
    const CONST = await require("var/const");
    const COM = await require("var/com");
    const app = await require("var/app");
    const MODS = await require("script/MODS");
    const Gui = (await require('lib/gui')).Frame;
    const dom = await require('lib/dommer');
    const NAME = await require('var/name');
    let runcmd = (str) => {
        if (str) {
            if (str[0] === "/"){
                if (str.length > 1) {
                    let cmds = str.slice(1).split(" ");
                    return COM[cmds[0]](...cmds.slice(1));
                }
            } else {    
                return eval(str);
            }
        }
    };
    aconsole.Aconsole = object.create({
        name: "Aconsole",
        constructor: function () {
            this.logs = [];
            this.fade = true;
            let self = this;
            let last_command = '/';
            this.typing = false;
            this.gui = new Gui();
            let keyH = async (e) => {
                if (e.key === "/") {
                    last_command = await MODS.get('lane-gui').getStr(self.gui, app.scence_top, last_command ? last_command : "/", dom.width()/2, dom.height() - 150);
                    try {
                        let result = runcmd(last_command);
                        if (typeof result === "object") {
                            self.print(JSON.stringify(result));
                        } else {
                            self.print(result ? result.toString() : "");
                        }
                    } catch (e) {
                        app.console.error(NAME.WRONG_COMMAND + ':' + last_command,e);
                    }
                } else if (e.key === "h") {
                    self.fade = !self.fade;
                }
            };
            this.controller = {
                keydown: (e) => {
                    if (!app.typing) {
                        keyH(e);
                        return false;
                    }
                }
            };
            this.drawer = (ctx, t) => {
                let canvas = ctx.canvas;
                let h = canvas.height - CONST.LOG_MARGIN_BOTTOM;
                let left = CONST.LOG_MARGIN_LEFT;
                self.logs.forEach((log,index) => {
                    h -= log.h + CONST.LOG_PADDING * log.h / log.size;
                    if (!log.fadding) {
                        if (log.fn) {
                            log.str = log.fn(() => {
                                log.time = calc.smallerThan(log.time,1000);
                            });
                        }
                        ctx.font = log.size + "px " + CONST.LOG_FONT_FAMILY;
                        ctx.globalAlpha = calc.smallerThan((log.time) / 1000, 1);
                        ctx.fillStyle = log.bc;
                        ctx.fillText(log.str, left + CONST.LOG_SHADOW_MX, h - log.h + CONST.LOG_SHADOW_MY);
                        ctx.fillStyle = log.fc;
                        ctx.fillText(log.str, left, h - log.h);
                        ctx.globalAlpha = 1;
                        log.time -= t;
                    }
                });
                self.logs.forEach((log,index) => {
                    if (self.fade) {
                        if (log.time < 0) {
                            if (log.h > 1) {
                                log.fadding = true;
                                log.h -= log.h / CONST.LOG_FADE_RATE;
                            } else {
                                self.logs.splice(index,1);            
                            }
                        } else {
                            log.h += (log.size - log.h) / CONST.LOG_FADE_RATE;
                        }
                    } else {
                        log.fadding = false;
                        log.h += (log.size - log.h) / CONST.LOG_FADE_RATE;
                        log.time = calc.largerThan(log.time, CONST.LOG_DELETE_TIMEOUT);
                    }
                });
                self.gui.drawer(ctx, t);
            }
        },
        proto: {
            println: function (str,time = CONST.LOG_DELETE_TIMEOUT,fc = CONST.LOG_FONT_COLOR,bc = CONST.LOG_BACKGROUND_COLOR,size = CONST.LOG_FONT_SIZE) {
                this.logs.unshift({
                    time:time,
                    maxtime:time,
                    str:str,
                    fc:fc,
                    bc:bc,
                    h:0,
                    size:size
                });
            },
            print: function (str, ...args) {
                if (str) {            
                    str.toString().split("\n").forEach((str) => {
                        this.println(str, ...args);
                    });
                }
            },
            printFn: function (fn,time = CONST.LOG_DELETE_TIMEOUT,fc = CONST.LOG_FONT_COLOR,bc = CONST.LOG_BACKGROUND_COLOR,size = CONST.LOG_FONT_SIZE) {
                this.logs.unshift({
                    time:time,
                    maxtime:time,
                    fn:fn,
                    fc:fc,
                    bc:bc,
                    h:0,
                    size:size
                });
            },
            error: function (str, e) {
                this.print(str + '\n[ERROR]' + e.toString(), 10000, '#f57900');
            }
        }
    });
};
