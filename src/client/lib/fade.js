/*
 *
 * 用于在场景切换之间加入渐变效果
 *
 */

;moudle = async function (fade) {
    const CONST = await require('var/const');
    fade.speed = CONST.FADE_SPEED;
    fade.mask_color = CONST.FADE_MASK_COLOR;
    fade.out = (scence, color = fade.mask_color, speed = fade.speed) => {
        return new Promise((res) => {
            let alpha = 0;
            let tick = (ctx, t) => {
                ctx.globalAlpha = alpha;
                ctx.fillStyle = color;
                ctx.fillRect(0,0,ctx.canvas.width,ctx.canvas.height);
                ctx.globalAlpha = 1;
                alpha += speed * t;
                if (alpha > 1) {
                    res();
                    scence.exitDrawer(tick);
                }
            }
            scence.pushDrawer(tick);
        });
    }
    fade.in = (scence, color = fade.mask_color, speed = fade.speed) => {
        return new Promise((res) => {
            let alpha = 1;
            let tick = (ctx, t) => {
                ctx.globalAlpha = alpha;
                ctx.fillStyle = color;
                ctx.fillRect(0,0,ctx.canvas.width,ctx.canvas.height);
                ctx.globalAlpha = 1;
                alpha -= speed * t;
                if (alpha < 0) {
                    res();
                    scence.exitDrawer(tick);
                }
            }
            scence.pushDrawer(tick);
        });
    }
};
