/*
~
~ GUI库！！！！！无敌！！！！！！
~
*/

;moudle = async function (gui) {
try{
    const
    object = await require('lib/object'),
    array = await require('lib/array'),
    app = await require('var/app'),
    NAME = await require('var/name'),
    changeView = (await require('script/change-view')).changeView,
    collision = await require('lib/collision');
    let checkIn = (elem, e) => {
        if (!elem._in) {
            elem._in = true;
            elem.onmousein(elem, e);
        }
    };
    let checkOut = (elem, e) => {
        if (elem._in) {
            elem._in = false;
            elem.onmouseout(elem, e);
        }
    };
    let doTranslate = (e) => {
        let pos = changeView(e);
        return {
            x: pos[0],
            y: pos[1]
        };
    }
    gui.Elements = new Map();
    gui.signElem = (name, elemClass) => {
        gui.Elements.set(name, elemClass);
    };
    gui.Frame = object.create({
        name: "Frame",
        constructor: function () {
            this.elems = [];
            this.handles = {};
            let self = this;
            this.drawer = (ctx, t) => {
                for (let elem of this.elems) {
                    elem.drawH(elem, ctx, t);
                }
            };
            this.controller = {
                mousedown: (e) => {
                    e = doTranslate(e);
                    for (let elem of this.elems.filter((elem) => elem.collisible)) {
                        if (collision.beInFn(e, elem)) {
                            checkIn(elem, e);
                            elem.onmousedown(elem, e);
                            return false;
                        } else {
                            checkOut(elem, e);
                        }
                    }
                },
                mouseup: (e) => {
                    e = doTranslate(e);
                    for (let elem of this.elems.filter((elem) => elem.collisible)) {
                        if (collision.beInFn(e, elem)) {
                            checkIn(elem, e);
                            elem.onmouseup(elem, e);
                        } else {
                            checkOut(elem, e);
                        }
                    }
                },
                mousemove: (e) => {
                    e = doTranslate(e);
                    for (let elem of this.elems.filter((elem) => elem.collisible)) {
                        if (collision.beInFn(e, elem)) {
                            checkIn(elem, e);
                            elem.onmousemove(elem, e);
                        } else {
                            checkOut(elem, e);
                        }
                    }
                }
            };
        },
        proto: {
            loadData: function (data) {
                try {
                    let elems = [];
                    for (let item of data) {
                        let elem = new (gui.Elements.get(item.name))(...item.args);
                        elem.gui = this;
                        elems.push(elem);
                        this.elems.push(elem);
                    }
                    return elems;
                } catch (e) {
                    app.console.error(NAME.GUI_FAILED_TO_LOAD_ELEMENT_NAMED + ':' + item.name, e);
                }
            },
            loadFn: function (data) {
                try {
                    for (let [handle, fn] of Object.entries(data)) {
                        this.handles[handle] = fn;
                    }
                } catch (e) {
                    app.console.error(NAME.GUI_FAILED_TO_LOAD_FUNCTION_NAMED + ':' + handle, e);
                }
            },
            callFn: async function (handle, elem, ...args) {
                try {
                    return await this.handles[handle](elem, ...args);
                } catch (e) {
                    app.console.error(NAME.GUI_FAILED_TO_CALL_FUNCTION_NAMED + ':' + handle, e);
                }
            },
            deleteElem: function (...elems) {
                try {
                    for (let elem of elems) {                
                        array.delete(this.elems, elem);
                    }
                } catch (e) {
                    app.console.error(NAME.GUI_FAILED_TO_DELETE_ELEMERNT_NAMED + ':' + elem.name, e);
                }
            },
            deleteFn: function (handle) {
                try {
                    this.handles[handle] = null;
                } catch (e) {
                    app.console.error(NAME.GUI_FAILED_TO_DELETE_FUNCTION_NAMED + ':' + handle, e);
                }
            }
        }
    });
} catch (e) {
    app.console.error(NAME.GUI_ERROR/UNKNOWN_POSITION + handle, e);
}
};
