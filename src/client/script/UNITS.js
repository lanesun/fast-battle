/*
~
~ 单位的管理库
~
*/

;moudle = async function (self) {
    self.units = new Map();
    self.get = (id) => {
        return self.units.get(id);
    };
    self.signUnit = (id, unit) => {
        unit.prototype.id = id;
        self.units.set(id, unit);
    };
};
