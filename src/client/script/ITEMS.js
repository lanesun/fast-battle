/*
~
~ 物品的管理库
~
*/

;moudle = async function (self) {
    self.items = new Map();
    self.get = (id) => {
        return self.items.get(id);
    };
    self.signItem = (id, item) => {
        item.prototype.id = id;
        self.items.set(id, item);
    };
};
