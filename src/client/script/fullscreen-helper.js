/*
~
~ 全屏API
~
*/
"use strict";

;moudle = (helper) => {
    helper.isFullscreen = () => {
        return document.fullscreenElement ? true : false;
    };
    helper.requestFullscreen = () => {
        return document.body.requestFullscreen();
    };
    helper.exitFullscreen = () => {
        return document.exitFullscreen();
    };
};
