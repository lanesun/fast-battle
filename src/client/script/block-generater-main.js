/*
~
~ 图块生成器的main函数
~
*/

const BOX_WIDTH = 400;
const BUTTON_HEIGHT = 103;

(async function () {
    const
    dom = await require('lib2/dommer'),
    calc = await require('lib2/calc');
    let canvas = dom.id('canvas');
    let Img = null;
    let ctx = canvas.getContext('2d');
    let scale = 1;
    let canvas_x = 0;
    let canvas_y = 0;
    let scale_sx = 0;
    let scale_sy = 0;
    let mouse = {
        x: 0,
        y: 0,
        rb: false,
        cb: false,
        lb: false,
        selecting: false,
        sx1: 0,
        sy1: 0,
        sx2: 0,
        sy2: 0,
    };
    ctx.imageSmoothingEnabled = false;
    let edit_box = dom.id('edit-box');
    let button_export = dom.id('button-export')
    let img_src_box = dom.id('img-src');
    let img_sx_box = dom.id('img-sx');
    let img_sy_box = dom.id('img-sy');
    let img_sw_box = dom.id('img-sw');
    let img_sh_box = dom.id('img-sh');
    let img_dw_box = dom.id('img-dw');
    let img_dh_box = dom.id('img-dh');
    let block_name_box = dom.id('block-name');
    let block_display_name_box = dom.id('block-display-name');
    let block_allowtype_land = dom.id('block-allowtype-land');
    let block_allowtype_land_water = dom.id('block-allowtype-land-water');
    let block_allowtype_water = dom.id('block-allowtype-water');
    let block_allowtype_air = dom.id('block-allowtype-air');
    let output_box = dom.id('output-box');
    
    let resize = () => {
        edit_box.style.top = 0;
        edit_box.style.left = 0;
        edit_box.style.height = dom.height() - BUTTON_HEIGHT + 'px';
        edit_box.style.width = BOX_WIDTH + 'px';
        button_export.style.bottom = 0;
        button_export.style.left = 0;
        button_export.style.height = BUTTON_HEIGHT + 'px';
        button_export.style.lineHeight = BUTTON_HEIGHT + 'px';
        button_export.style.width = BOX_WIDTH + 'px';
        canvas_x = (dom.width() + BOX_WIDTH)/2 - canvas.width / 2 + scale_sx;
        canvas_y = dom.height()/2 - canvas.height / 2 + scale_sy;
        canvas.style.top = canvas_y + 'px';
        canvas.style.left = canvas_x + 'px';
        canvas.style.transform = 'scale(' + scale + ')';
    };
    
    let changeImage = () => {
        let img = new Image();
        img.onload = async () => {
            canvas.width = img.width * 10;
            canvas.height = img.height * 10;
            Img = await createImageBitmap(img,0,0,img.width,img.height,
            {
                resizeWidth: img.width * 10,
                resizeHeight: img.height * 10,
                resizeQuality: 'pixelated'
            });
            scale_sx = 0;
            scale_sy = 0;
            scale = 1;
            resize();
        }
        img.src = img_src_box.innerText;
    };
    img_src_box.onkeydown = (e) => {
        if (e.key === 'Enter') {
            changeImage();
            return false;
        }
    };
    let drawH = () => {
        ctx.clearRect(0,0,canvas.width,canvas.height);
        if (Img) ctx.drawImage(Img, 0, 0);
        for (let x = 0; x <= canvas.width; x += 10) {
            ctx.beginPath();
            ctx.moveTo(x, 0);
            ctx.lineTo(x, canvas.height);
            ctx.lineWidth = 2;
            ctx.strokeStyle = '#0004';
            ctx.stroke();
        }
        for (let y = 0; y <= canvas.height; y += 10) {
            ctx.beginPath();
            ctx.moveTo(0, y);
            ctx.lineTo(canvas.width, y);
            ctx.lineWidth = 2;
            ctx.strokeStyle = '#0004';
            ctx.stroke();
        }
        for (let x = 0; x <= canvas.width; x += 160) {
            ctx.beginPath();
            ctx.moveTo(x, 0);
            ctx.lineTo(x, canvas.height);
            ctx.lineWidth = 2;
            ctx.strokeStyle = '#00f';
            ctx.stroke();
        }
        for (let y = 0; y <= canvas.height; y += 160) {
            ctx.beginPath();
            ctx.moveTo(0, y);
            ctx.lineTo(canvas.width, y);
            ctx.lineWidth = 2;
            ctx.strokeStyle = '#00f';
            ctx.stroke();
        }
        if (mouse.selecting) {
            ctx.strokeStyle = '#ff0';
            ctx.lineWidth = 4;
            ctx.strokeRect(mouse.sx1 * 10, mouse.sy1 * 10, (mouse.sx2 - mouse.sx1) * 10, (mouse.sy2 - mouse.sy1) * 10)
        }
        requestAnimationFrame(drawH);
    };
    drawH();
    resize();
    window.oncontextmenu = (e) => {
        return false;
    };
    window.onresize = resize;
    window.onwheel = (e) => {
        if (e.clientX < BOX_WIDTH) return;
        if (scale <= 0.5 && e.deltaY > 0) return;
        let s = -0.4 * (e.deltaY > 0 ? 1 : -1);
        scale_sx += (canvas_x + canvas.width / 2 - e.clientX) * s / scale;
        scale_sy += (canvas_y + canvas.height / 2 - e.clientY) * s / scale;
        scale = calc.largerThan(scale + s, 0.5);
        resize();
    };
    canvas.onmousedown = (e) => {
        if (e.button === 0) {
            mouse.lb = true;
            mouse.selecting = true;
            mouse.sx1 = Math.floor(e.offsetX / 160) * 16;
            mouse.sy1 = Math.floor(e.offsetY / 160) * 16;
            mouse.sx2 = Math.floor(e.offsetX / 160 + 1) * 16;
            mouse.sy2 = Math.floor(e.offsetY / 160 + 1) * 16;
        }
        if (e.button === 1) {
            mouse.cb = true;
        }
        if (e.button === 2) {
            mouse.rb = true;
        }
    };
    canvas.onmouseup = (e) => {
        mouse.lb = mouse.cb = mouse.rb = false;
        mouse.selecting = false;
        img_sx_box.innerText = mouse.sx1;
        img_sy_box.innerText = mouse.sy1;
        img_sw_box.innerText = mouse.sx2 - mouse.sx1;
        img_sh_box.innerText = mouse.sy2 - mouse.sy1;
    };
    canvas.onmouseleave = (e) => {
        mouse.lb = mouse.cb = mouse.rb = false;
        mouse.selecting = false;
    };
    canvas.onmousemove = (e) => {
        if (mouse.rb) {
            scale_sx += e.movementX;
            scale_sy += e.movementY;
            resize();
        }
        if (mouse.selecting) {
            mouse.sx2 = Math.floor(e.offsetX / 160 + 1) * 16;
            mouse.sy2 = Math.floor(e.offsetY / 160 + 1) * 16;
        }
    };
    
    button_export.onclick = () => {
        let ary = [];
        if (block_allowtype_land.checked) {
            ary.push('land');
        }
        if (block_allowtype_land_water.checked) {
            ary.push('land&water');
        }
        if (block_allowtype_water.checked) {
            ary.push('water');
        }
        if (block_allowtype_air.checked) {
            ary.push('air');
        }
        let allowType = JSON.stringify(ary);
        let str = `;moudle = async function (self) {
    const IMGS = await require('script/IMGS'),
    object = await require('lib/object'),
    CONST = await require('var/const'),
    NAME = await require('var/name'),
    Block = (await require('mods/lane-block/Block')).obj;
    IMGS.add('${img_src_box.innerText}');
    
    self.getClass = async () => {
        return object.create({
            name: "${block_name_box.innerText}",
            extend: Block,
            constructor: function (...args) {
                Block.call(this, ...args)
            },
            proto: {
                display_name: '${block_display_name_box.innerText}',
                allowType: new Set(${allowType}),
                img: await IMGS.get('${img_src_box.innerText}',${img_sx_box.innerText},${img_sy_box.innerText},${img_sw_box.innerText},${img_sh_box.innerText},${img_dw_box.innerText} * CONST.BLOCK_DISPLAY_SIZE,${img_dh_box.innerText} * CONST.BLOCK_DISPLAY_SIZE)
            }
        });
    };
};`
        output_box.innerText = str;
        window.getSelection().selectAllChildren(output_box);
        document.execCommand('copy');
        button_export.innerText = '已复制';
        button_export.classList.add('alert');
        window.setTimeout(() => {
            button_export.innerText = '导出并复制';
            button_export.classList.remove('alert');
        },1000);
        return false;
    };
})();
