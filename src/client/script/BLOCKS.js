/*
~
~ 方块的管理库
~
*/

;moudle = async function (self) {
    self.blocks = new Map();
    self.get = (id) => {
        return self.blocks.get(id);
    };
    self.signBlock = (id, block) => {
        block.prototype.id = id;
        self.blocks.set(id, block);
    };
};
