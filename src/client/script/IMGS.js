/*
~
~ 位图加载脚本
~ 转换成ImageBitmap再使用更快速
~
*/

;moudle = async function (IMGS) {
    let resources = new Map();
    let canvas = new OffscreenCanvas(0,0);
    let context = canvas.getContext('2d');
    context.imageSmoothingEnabled = false;
    let preloadList = [];
    let load = (src) => {
        return new Promise((resolve) => {
            let img = new Image();
            img.onload = () => {
                IMGS.loaded++;
                resolve();
            }
            img.src = src;
            resources.set(src, img);
        });
    };
    IMGS.add = (src) => {
        preloadList.push(src);
    };
    IMGS.loaded = 0;
    IMGS.total = () => preloadList.length;
    IMGS.load = () => {
        let promises = [];
        for (let src of preloadList) {
            promises.push(load(src));
        }
        return Promise.all(promises);
    };
    IMGS.get = (src, sx, sy, sw, sh, w, h) => {
        canvas.width = w;
        canvas.height = h;
        context.imageSmoothingEnabled = false;
        context.clearRect(0,0,w,h);
        context.drawImage(resources.get(src), sx, sy, sw, sh, 0, 0, w, h);
        return createImageBitmap(canvas,0,0,w,h);
    };
    IMGS.getRaw = (src, sx, sy, sw, sh, w, h) => {
        return createImageBitmap(
            resources.get(src),
            sx,
            sy,
            sw,
            sh,
            {
                resizeWidth: w,
                resizeHeight: h,
                resizeQuality: 'pixelated'
            });
    };
};
