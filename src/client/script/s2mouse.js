/*
~
~ 适用于s2painter对象的鼠标控制器
~
*/

;moudle = async function (s2mouse) {
    const changeView = (await require('script/change-view')).changeView,
    eventer = await require('lib/eventer'),
    object = await require('lib/object'),
    calc = await require('lib/calc');
    s2mouse.S2Mouse = object.create({
        name: 'S2Mouse',
        extend: eventer.EventTarget,
        constructor: function (s2painter) {
            eventer.EventTarget.call(this);
            this.s2painter = s2painter;
            this.x = 0;
            this.y = 0;
            this.rb = false;
            this.lb = false;
            this.inside = false;
            this.selecting = false;
            this.select_box = {x1:0, y1:0, x2:0, y2:0};
            let self = this;
            this.controller = {
                mousedown: (e) => {
                    self.inside = true;
                    [self.x, self.y] = changeView(e);
                    if (e.button === 0) {
                        self.lb = true;
                        self.selecting = true;
                        self.select_box.x1 = self.select_box.x2 = calc.transScreenX(self.s2painter,self.x,self.y);
                        self.select_box.y1 = self.select_box.y2 = calc.transScreenY(self.s2painter,self.x,self.y);
                        self.trigger('down');
                    }
                    if (e.button === 2) {
                        self.rb = true;
                    }
                },
                mouseup: (e) => {
                    self.inside = true;
                    [self.x, self.y] = changeView(e);
                    self.trigger('up');
                    if (e.button === 0) {
                        self.lb = false;
                        self.selecting = false;
                    }
                    if (e.button === 2) {
                        self.rb = false;
                    }
                },
                mousemove: (e) => {
                    self.inside = true;
                    if (self.rb) {
                        self.s2painter.position.x -= calc.transX(e.movementX, e.movementY);
                        self.s2painter.position.y -= calc.transY(e.movementX, e.movementY); 
                    }
                    [self.x, self.y] = changeView(e);
                    if (self.selecting) {
                        self.select_box.x2 = calc.transScreenX(self.s2painter,self.x,self.y);
                        self.select_box.y2 = calc.transScreenY(self.s2painter,self.x,self.y);
                    }
                    self.trigger('move');
                },
                mouseleave: (e) => {
                    self.inside = false;
                    self.selecting = false;
                    [self.x, self.y] = changeView(e);
                }
            };
        },
        proto: {
            getRealX: (self) => calc.transScreenX(self.s2painter, self.x, self.y),
            getRealY: (self) => calc.transScreenY(self.s2painter, self.x, self.y)
        }
    });
};
