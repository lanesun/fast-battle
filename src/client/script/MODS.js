/*
~
~ 模组加载脚本
~ 万恶之源（好游戏怎么可能没有MODS呢）
~
*/

;moudle = async function (MODS) {
    let modlist = (await require('var/modlist')).data;
    let mods = [];
    MODS.mods = {};
    MODS.init = async () => {
        for (let modname of modlist) {
            mods.push({
                name: modname,
                self: await require('mods/' + modname)
            });
        }
    };
    let load = async (mod) => {
        await mod.self.load();
        MODS.mods[mod.name] = mod.self; 
        MODS.loaded++;
    };
    MODS.loaded = 0;
    MODS.total = () => mods.length;
    MODS.load = () => {
        let promises = [];
        for (let mod of mods) {
            promises.push(load(mod));
        }
        return Promise.all(promises);
    };
    MODS.get = (name) => {
        return MODS.mods[name];
    };
};
