/*
~
~ 正常视角的绘制API
~
*/

;moudle = async function (painter) {
    const
    object = await require('lib/object');
    painter.Painter = object.create({
        name: "Painter",
        constructor: function () {
            this.context = null;
        },
        proto: {
            drawImage: function (img, x, y) {
                this.context.drawImage(img, Math.floor(x), Math.floor(y));
            },
            clear: function () {
                this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
            },
            init: function (cav) {
                this.context = cav.getContext('2d');
                this.context.imageSmoothingEnabled = false;
            }
        }
    });
    painter.drawImage = (ctx, img, x, y) => {
        ctx.drawImage(img, Math.floor(x), Math.floor(y));
    };
    painter.clear = (ctx) => {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    };
    painter.fill = (ctx, color) => {
        ctx.fillStyle = color;
        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    };
};
