/*
~
~ 游戏地图对象
~
*/

;moudle = async function (self) {
    const
    object = await require('lib/object'),
    dom = await require('lib/dommer'),
    calc = await require("lib/calc"),
    CONST = await require('var/const'),
    BLOCKS = await require('script/BLOCKS');
    self.GameMap = object.create({
        name: 'GameMap',
        constructor: function (width, height, block_real_size = CONST.BLOCK_REAL_SIZE, block_collosion_size = CONST.BLOCK_COLLOSION_SIZE, block_disply_size = CONST.BLOCK_DISPLAY_SIZE, id = 'Default', blocks, units) {
            this.width = width;
            this.height = height;
            this.id = id;
            this.block_real_size = block_real_size;
            this.block_disply_size = block_disply_size;
            this.block_collosion_size = block_collosion_size;
            this.blocks = blocks ? this.loadBlocks(blocks) : [];
            this.units = units ? this.loadUnits(units) : [];
        },
        proto: {
            sideBlock: {
                id: '',
                allowType: new Set(),
            },
            loadBlocks: function (blocks) {
                this.blocks = [];
                let i = 0;
                for (let name of blocks) {
                    let [x, y] = this.getPos(i);
                    x = x * this.block_real_size;
                    y = y * this.block_real_size;
                    this.blocks.push(new BLOCKS.get(name)(x, y));
                    i++;
                }
            },
            fillBlocks: function (name) {
                for (let i = 0; i < this.width * this.height; i++) {
                    let [x, y] = this.getPos(i);
                    x = x * this.block_real_size;
                    y = y * this.block_real_size;
                    this.blocks.push(new (BLOCKS.get(name))(x, y));
                }
            },
            setBlock: function (x, y, name) {
                if (x >= 0 && x < this.width && y >= 0 && y < this.height) {
                    this.blocks[x + y * this.width] = new (BLOCKS.get(name))(x * this.block_real_size, y * this.block_real_size);
                }
            },
            getPos: function (i) {
                return [i % this.width, Math.floor(i / this.width)];
            },
            getBlock: function (x, y) {
                if (x >= 0 && x < this.width && y >= 0 && y < this.height) {
                    return this.blocks[x + y * this.width];
                } else {
                    return this.sideBlock;
                }
            },
            getBlockAt: function (x, y) {
                x = calc.floorToBlock(x);
                y = calc.floorToBlock(y);
                if (x >= 0 && x < this.width && y >= 0 && y < this.height) {
                    return this.blocks[x + y * this.width];
                } else {
                    return this.sideBlock;
                }
            },
            drawBlocks: (self, s2p) => {
                let x1 = calc.inBetween(0,calc.floorToBlock(calc.transScreenX(s2p, 0, 0)),self.width - 1);
                let y1 = calc.inBetween(0,calc.floorToBlock(calc.transScreenY(s2p, dom.width(), 0)),self.height - 1);
                let x2 = calc.inBetween(0,calc.floorToBlock(calc.transScreenX(s2p, dom.width(), dom.height())),self.width - 1);
                let y2 = calc.inBetween(0,calc.floorToBlock(calc.transScreenY(s2p, 0, dom.height())),self.height - 1);
                for (let x = x1; x <= x2; x++) {//这里可以优化，提高至只绘制一半的区域
                    for (let y = y1; y <= y2; y++) {
                        let block = self.getBlock(x, y);
                        block.drawH(block, s2p, self);
                    }
                }
            },
            drawUnits: (self, s2p) => {
                let x1 = calc.inBetween(0,calc.floorToBlock(calc.transScreenX(s2p, 0, 0)),self.width - 1);
                let y1 = calc.inBetween(0,calc.floorToBlock(calc.transScreenY(s2p, dom.width(), 0)),self.height - 1);
                let x2 = calc.inBetween(0,calc.floorToBlock(calc.transScreenX(s2p, dom.width(), dom.height())),self.width - 1);
                let y2 = calc.inBetween(0,calc.floorToBlock(calc.transScreenY(s2p, 0, dom.height())),self.height - 1);
                self.units.sort((unit1,unit2) => (unit1.x + unit1.y  - unit2.x - unit2.y));
                for (let unit of self.units) {
                    if (unit.x > x1 * self.block_real_size && unit.x < x2 * self.block_real_size && unit.y > y1 * self.block_real_size && unit.y < y2 * self.block_real_size) {
                        unit.lowerDrawH(unit, s2p);
                    }
                }
                for (let unit of self.units) {
                    if (unit.x > x1 * self.block_real_size && unit.x < x2 * self.block_real_size && unit.y > y1 * self.block_real_size && unit.y < y2 * self.block_real_size) {
                        unit.drawH(unit, s2p);
                    }
                }
            },
            drawH: (self, s2p) => {
                let x1 = calc.inBetween(0,calc.floorToBlock(calc.transScreenX(s2p, 0, 0)),self.width - 1);
                let y1 = calc.inBetween(0,calc.floorToBlock(calc.transScreenY(s2p, dom.width(), 0)),self.height - 1);
                let x2 = calc.inBetween(0,calc.floorToBlock(calc.transScreenX(s2p, dom.width(), dom.height())),self.width - 1);
                let y2 = calc.inBetween(0,calc.floorToBlock(calc.transScreenY(s2p, 0, dom.height())),self.height - 1);
                for (let x = x1; x <= x2; x++) {//这里可以优化，提高至只绘制一半的区域
                    for (let y = y1; y <= y2; y++) {
                        let block = self.getBlock(x, y);
                        block.drawH(block, s2p, self);
                    }
                }
                self.units.sort((unit1,unit2) => (unit1.x + unit1.y  - unit2.x - unit2.y));
                for (let unit of self.units) {
                    if (unit.x > x1 * self.block_real_size && unit.x < x2 * self.block_real_size && unit.y > y1 * self.block_real_size && unit.y < y2 * self.block_real_size) {
                        unit.lowerDrawH(unit, s2p);
                    }
                }
                for (let unit of self.units) {
                    if (unit.x > x1 * self.block_real_size && unit.x < x2 * self.block_real_size && unit.y > y1 * self.block_real_size && unit.y < y2 * self.block_real_size) {
                        unit.drawH(unit, s2p);
                    }
                }
            },
            logicH: (self, t) => {
                for (let unit of self.units) {
                    if (unit.logical) {
                        unit.logicH(unit, t, self);
                    }
                    if (unit.thinking) {
                        unit.ai(unit, t, self);
                    }
                }
                for (let unit of self.units) {
                    if (unit.collisible) {
                        let block = self.getBlock(calc.floorToBlock(unit.x), calc.floorToBlock(unit.y));
                        if (block.collisionH) {
                            block.collisionH(block, unit, self);
                        }
                        for (let target of self.units) {
                            if (unit !== target) {
                                unit.collisionH(unit, target);
                            }
                        }
                    }
                }
                self.units.forEach((unit, index) => {
                    if (unit.deleted) {
                        self.units.splice(index, 1);
                    }
                });
            },
            getJSONData: (self) => {
                let units = [];
                let blocks = [];
                let width = self.width;
                let height = self.height;
                let id = self.id;
                let block_real_size = self.block_real_size;
                let block_collosion_size = self.block_collosion_size;
                let block_disply_size = self.block_disply_size;
                for (let block of self.blocks) {
                    blocks.push(block.id);
                }
                for (let unit of self.units) {
                    units.push(unit.id);
                }
                return JSON.stringify({
                    width: width,
                    height: height,
                    id: id,
                    block_real_size: block_real_size,
                    block_collosion_size: block_collosion_size,
                    block_disply_size: block_disply_size,
                    units: units,
                    blocks: blocks
                });
            }
        }
    });
};
