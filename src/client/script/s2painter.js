/*
~
~ 顶视角的绘制API
~
*/

;moudle = async function (s2painter) {
    let canvas = null;
    let ctx = null;
    const NU_1S2 = 1/(2**(0.5));
    const NU_S2 = 2**(0.5);
    const untransX = (x, y) => x;
    const untransY = (x, y) => y;
    const untransScreenX = (self, x, y) => untransX(x - self.position.x, y - self.position.y) + self.canvas.width/2;
    const untransScreenY = (self, x, y) => untransY(x - self.position.x, y - self.position.y) + self.canvas.height/2;
    const
    object = await require('lib/object');
    s2painter.S2Painter = object.create({
        name: "S2Painter",
        constructor: function () {
            this.context = null;
            this.canvas = null;
            this.position = {
                x: 0,
                y: 0
            };//这是视图中央在地图上的位置
        },
        proto: {
            drawImage: function (img, x, y, mx = 0, my = 0, trans) {
                x = untransScreenX(this,x,y);
                y = untransScreenY(this,x,y)
                if (trans) {
                    this.context.save();
                    if (trans.rotate) {
                        this.context.transform(
                            Math.cos(trans.rotate),
                            Math.sin(trans.rotate),
                            -Math.sin(trans.rotate),
                            Math.cos(trans.rotate),
                            x - Math.cos(trans.rotate) * x + Math.sin(trans.rotate) * y,
                            y - Math.sin(trans.rotate) * x - Math.cos(trans.rotate) * y
                        );
                    }
                }
                this.context.drawImage(img, Math.floor(x + mx - img.width/2), Math.floor(y + my - img.height));
                if (trans) {
                    this.context.restore();
                }
            },
            clear: function () {
                this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            },
            load: function (ctx) {
                this.context = ctx;
                this.canvas = this.context.canvas;
            },
            fill: function (color) {
                this.context.fillStyle = color;
                this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
            },
            strokeRect: function (color, alpha, width, x1, y1, x2, y2, mx = 0, my = 0) {
                let
                rx1 = untransScreenX(this,x1,y1) + mx,
                ry1 = untransScreenY(this,x1,y1) + my,
                rx2 = untransScreenX(this,x2,y1) + mx,
                ry2 = untransScreenY(this,x2,y1) + my,
                rx3 = untransScreenX(this,x2,y2) + mx,
                ry3 = untransScreenY(this,x2,y2) + my,
                rx4 = untransScreenX(this,x1,y2) + mx,
                ry4 = untransScreenY(this,x1,y2) + my;
                this.context.strokeStyle = color;
                this.context.lineWidth = width;
                this.context.lineCap="butt";
                this.context.lineJoin="miter";
                this.context.globalAlpha = alpha;
                this.context.miterLimit = Infinity;
                this.context.beginPath();
                this.context.moveTo(rx1, ry1);
                this.context.lineTo(rx2, ry2);
                this.context.lineTo(rx3, ry3);
                this.context.lineTo(rx4, ry4);
                this.context.closePath();
                this.context.stroke();
                this.context.globalAlpha = 1;
            },
        }
    });
};
