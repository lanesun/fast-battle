/*
~
~ 程序的启动入口
~ 对对对，这才是真正的main函数。。。
~
*/

;moudle = async () => {
    const
    dom = await require('lib/dommer'),
    app = await require('var/app'),
    scence = await require('lib/scence'),
    MODS = await require('script/MODS');
    //scale = await require('lib/scale');
    
    const canvases = [
        dom.id('canvas'),
        dom.id('canvas-gui'),
        dom.id('canvas-top'),
    ];
    
    (await require('script/resize-helper')).init(canvases);
    //scale.init(dom.id('canvas-0'));
    app.scence = new scence.Scence();
    app.scence_gui = new scence.Scence();
    app.scence_top = new scence.Scence();
    app.scence.init(canvases[0]);
    app.scence_gui.init(canvases[1]);
    app.scence_top.init(canvases[2]);
    app.console = new (await require("lib/aconsole")).Aconsole();
    
    window.oncontextmenu = (e) => {
        return false;
    };
    
    app.scence_top.pushDrawer(app.console.drawer);
    app.scence_top.pushController(app.console.controller);
    
    await MODS.init();
    
    await Promise.all([
        require('scence/start-screen'),
        require('scence/main-screen'),
        require('scence/main-game'),
        require('scence/map-editor')
    ]);
    
    await app.scence_gui.pushScence('start-screen');
};
