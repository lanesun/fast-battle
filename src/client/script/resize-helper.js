/*
~
~ 在窗口改变大小时自动调整Canvas大小
~
*/
"use strict";

;moudle = (helper) => {
    helper.init = (canvases) => {
        let resize_handle = () => {
            canvases.forEach((canvas) => {
			    canvas.width = 1920;
                canvas.height = 1080;
            });
        };
        resize_handle();
    }
};
