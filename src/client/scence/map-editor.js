;moudle = async function (self) {
    try {
        const
        dom = await require('lib/dommer'),
        wait = (await require('lib/timer')).wait,
        painter = await require('script/painter'),
        S2Painter = (await require('script/s2painter')).S2Painter,
        S2Mouse = (await require('script/s2mouse')).S2Mouse,
        fade = await require('lib/fade'),
        calc = await require('lib/calc'),
        app = await require('var/app'),
        Gui = (await require('lib/gui')).Frame,
        FLAG = await require('var/flag'),
        fullscreen_helper = await require('script/fullscreen-helper'),
        GameMap = (await require('script/GameMap')).GameMap,
        IMGS = await require('script/IMGS'),
        BLOCKS = await require('script/BLOCKS'),
        NAME = await require('var/name'),
        CONST = await require('var/const'),
        MODS = await require('script/MODS'),
        DATAS = await require('scence/map-editor-data');
        let lane_gui = null;
        let main_view = null;
        let blocks_view = null;
        let mapsize_box = null;
        let selectblock_box = null;
        let selectblock_name = CONST.DEFAULT_BLOCK;
        let s2painter = null;
        let s2mouse = null;
        let position_popup = null;
        
        let changeFullscreen = () => {
            if (!fullscreen_helper.isFullscreen()) {
                fullscreen_helper.requestFullscreen();
            } else {
                fullscreen_helper.exitFullscreen();
            }
        };
        
        
        let showBlocks = (data) => {
            let x = 0;
            let y = 0;
            for (let [name,Block] of BLOCKS.blocks) {
                data.unshift({
                    name: 'lane-gui-img-box-popup',
                    args: [Block.prototype.img, x * 120 + 48, y * 120 + 250, 64, 64, 'set_block', name, Block.prototype.display_name, 32],
                });
                y++;
                if (y > 5) {
                    x++;
                    y = 0;
                }
            }
            return data;
        };
        
        let setBlock = (elem, name) => {
            selectblock_name = name;
            selectblock_box.img = BLOCKS.get(name).prototype.img;
        };
        
        let getIn = async () => {
            app.scence_gui.pushDrawer(gui.drawer);
            app.scence.pushDrawer(mapdrawer);
            await fade.in(app.scence_gui);
            app.scence_top.pushController(gui.controller);
            app.scence_top.pushController(s2mouse.controller);
        };
        
        let getOut = async () => {
            app.scence_top.exitController(gui.controller);
            app.scence_top.exitController(s2mouse.controller);
            await fade.out(app.scence_gui);
            app.scence_gui.exitDrawer(gui.drawer);
            app.scence.exitDrawer(mapdrawer);
        };
        
        let agl = 0;
        let mapdrawer = (ctx, t) => {
            painter.fill(ctx, "#ffffff");
            app.gamemap.drawBlocks(app.gamemap, s2painter);
            let x = calc.floorToBlock(s2mouse.getRealX(s2mouse)) * app.gamemap.block_real_size;
            let y = calc.floorToBlock(s2mouse.getRealY(s2mouse)) * app.gamemap.block_real_size;
            let cb = new (BLOCKS.get(selectblock_name))(x, y);
            cb.drawH(cb, s2painter, app.gamemap);
            if (s2mouse.selecting) {
                let x1 = calc.floorToBlock(s2mouse.select_box.x1) * app.gamemap.block_real_size;
                let y1 = calc.floorToBlock(s2mouse.select_box.y1) * app.gamemap.block_real_size;
                let x2 = calc.floorToBlock(s2mouse.select_box.x2) * app.gamemap.block_real_size;
                let y2 = calc.floorToBlock(s2mouse.select_box.y2) * app.gamemap.block_real_size;
                agl = calc.getAngle(agl + t * 0.01);
                s2painter.strokeRect('#000', 0.2, 8, Math.min(x1, x2), Math.min(y1, y2), Math.max(x1, x2) + app.gamemap.block_real_size, Math.max(y1, y2) + app.gamemap.block_real_size, -app.gamemap.block_disply_size/2, -app.gamemap.block_disply_size/2);
                s2painter.strokeRect('#ff0', 1, 8, Math.min(x1, x2), Math.min(y1, y2), Math.max(x1, x2) + app.gamemap.block_real_size, Math.max(y1, y2) + app.gamemap.block_real_size, -app.gamemap.block_disply_size/2, -app.gamemap.block_disply_size/2 - 12 - 6 * Math.sin(agl));
            }
            app.gamemap.drawUnits(app.gamemap, s2painter);
        };
        
        let setCurrentBlock = () => {
            let x = calc.floorToBlock(s2mouse.getRealX(s2mouse));
            let y = calc.floorToBlock(s2mouse.getRealY(s2mouse));
            if (x >= 0 && x < app.gamemap.width && y >= 0 && y < app.gamemap.height) {
                app.gamemap.setBlock(x, y, selectblock_name);
            }
        };
        
        let setRectBlock = () => {
            if (s2mouse.selecting) {
                let x1 = calc.inBetween(0, calc.floorToBlock(s2mouse.select_box.x1), app.gamemap.width - 1);
                let y1 = calc.inBetween(0, calc.floorToBlock(s2mouse.select_box.y1), app.gamemap.height - 1);
                let x2 = calc.inBetween(0, calc.floorToBlock(s2mouse.select_box.x2), app.gamemap.width - 1);
                let y2 = calc.inBetween(0, calc.floorToBlock(s2mouse.select_box.y2), app.gamemap.height - 1);
                for (let x = Math.min(x1, x2); x <= Math.max(x1, x2); x++) {
                    for (let y = Math.min(y1, y2); y <= Math.max(y1, y2); y++) {
                        app.gamemap.setBlock(x, y, selectblock_name);
                    }
                }
            }
        };
        
        let refreshPosition = () => {
            let x = calc.floorToBlock(s2mouse.getRealX(s2mouse));
            let y = calc.floorToBlock(s2mouse.getRealY(s2mouse));
            position_popup[1].x = s2mouse.x + 32;
            position_popup[1].y = s2mouse.y + 16;
            position_popup[0].x = s2mouse.x + 33;
            position_popup[0].y = s2mouse.y + 18;
            position_popup[0].str = '(' + x + ':' + y + ')';
            position_popup[1].str = '(' + x + ':' + y + ')';
        };
        
        let changeMapWidth = async () => {
            let num = parseInt(await lane_gui.getStr(gui, app.scence_top, app.gamemap.width, dom.width()/2 + 100, dom.height() - 160));
            if (num && !isNaN(num)) {
                app.gamemap = new GameMap(num, app.gamemap.height);
                app.gamemap.fillBlocks(CONST.DEFAULT_BLOCK);
                mapsize_box[0].str = NAME.WIDTH + ':' + app.gamemap.width;
                mapsize_box[0].rewidth(mapsize_box[0]);
            }
        };
        
        let changeMapHeight = async () => {
            let num = parseInt(await lane_gui.getStr(gui, app.scence_top, app.gamemap.height, dom.width()/2 + 500, dom.height() - 160));
            if (num && !isNaN(num)) {
                app.gamemap = new GameMap(app.gamemap.width, num);
                app.gamemap.fillBlocks(CONST.DEFAULT_BLOCK);
                mapsize_box[1].str = NAME.HEIGHT + ':' + app.gamemap.height;
                mapsize_box[1].rewidth(mapsize_box[1]);
            }
        };
        
        let saveGameMap = async () => {
            let name = await lane_gui.prompt(gui, app.scence_top, NAME.NAME_THE_MAP, app.gamemap.id, dom.width()/2, dom.height()/2);
            if (name && name.toString()) {
                try {
                    name = name.toString();
                    app.gamemap.id = name;
                    let data = app.gamemap.getJSONData(app.gamemap);
                    localStorage.setItem(CONST.$APP + '_MAP_' + name, data);
                    app.console.print(NAME.MAP_SAVE, 5000, '#729fcf');
                } catch (e) {
                    app.console.error(NAME.FAIL_TO_SAVE_MAP, e);
                }
            }
        };
        
        let gui = null;
        
        let main = async (file_name) => {
            lane_gui = MODS.get('lane-gui');
            gui = new Gui();
            main_view = gui.loadData(DATAS.data());
            selectblock_box = gui.loadData(DATAS.selectblock_box())[0];
            mapsize_box = gui.loadData(DATAS.mapsize_box());
            blocks_view = gui.loadData(showBlocks([]));
            position_popup = gui.loadData(DATAS.position_popup());
            gui.loadFn({
                change_fullscreen: changeFullscreen,
                set_block: setBlock,
                change_map_height: changeMapHeight,
                change_map_width: changeMapWidth,
                save_game_map: saveGameMap,
                close: async () => {
                    await getOut();
                    app.scence_gui.exitScence();
                }
            });
            s2painter = new S2Painter();
            s2painter.load(app.scence.context);
            s2painter.position.x = (app.gamemap.width/2 - 1) * app.gamemap.block_real_size;
            s2painter.position.y = (app.gamemap.height/2 - 1) * app.gamemap.block_real_size;
            s2mouse = new S2Mouse(s2painter);
            s2mouse.on('down', setCurrentBlock);
            s2mouse.on('up', setRectBlock);
            s2mouse.on('move', refreshPosition);
            await getIn();
        };
        
        app.scence_gui.signScence('map-editor', main);
        
    } catch (e) {
        app.console.error('',e);
    }
};
