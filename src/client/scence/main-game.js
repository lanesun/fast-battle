;moudle = async function (self) {
    try {
        const
        dom = await require('lib/dommer'),
        wait = (await require('lib/timer')).wait,
        painter = await require('script/painter'),
        S2Painter = (await require('script/s2painter')).S2Painter,
        S2Mouse = (await require('script/s2mouse')).S2Mouse,
        fade = await require('lib/fade'),
        calc = await require('lib/calc'),
        app = await require('var/app'),
        Gui = (await require('lib/gui')).Frame,
        FLAG = await require('var/flag'),
        GameMap = (await require('script/GameMap')).GameMap,
        IMGS = await require('script/IMGS'),
        BLOCKS = await require('script/BLOCKS'),
        UNITS = await require('script/UNITS'),
        NAME = await require('var/name'),
        CONST = await require('var/const'),
        MODS = await require('script/MODS'),
        DATAS = await require('scence/main-game-data');
        let lane_gui = null;
        let main_view = null;
        let s2painter = null;
        let s2mouse = null;
        
        let getIn = async () => {
            app.scence_gui.pushDrawer(gui.drawer);
            app.scence.pushDrawer(mapdrawer);
            await fade.in(app.scence_gui);
            app.scence_top.pushController(gui.controller);
            app.scence_top.pushController(s2mouse.controller);
        };
        
        let getOut = async () => {
            app.scence_top.exitController(gui.controller);
            app.scence_top.exitController(s2mouse.controller);
            await fade.out(app.scence_gui);
            app.scence_gui.exitDrawer(gui.drawer);
            app.scence.exitDrawer(mapdrawer);
        };
        
        let mapdrawer = (ctx, t) => {
            painter.fill(ctx, "#000000");
            app.gamemap.drawH(app.gamemap, s2painter);
            app.gamemap.logicH(app.gamemap, t);
        };
        
        let putGunMan = () => {
            let unit = new (UNITS.get('lane-tank2'))(s2mouse.getRealX(s2mouse),s2mouse.getRealY(s2mouse));
            if (Math.random() > 0.5) {
                unit.team = 1;
            } else {
                unit.team = 2;
            }
            app.gamemap.units.push(unit);
        };
        
        let gui = null;
        
        let main = async (file_name) => {
            lane_gui = MODS.get('lane-gui');
            gui = new Gui();
            main_view = gui.loadData(DATAS.data());
            gui.loadFn({
                close: async (elem) => {
                    await getOut();
                    app.scence_gui.exitScence();
                }
            });
            s2painter = new S2Painter();
            s2painter.load(app.scence.context);
            s2mouse = new S2Mouse(s2painter);
            s2mouse.on('down', putGunMan);
            await getIn();
        };
        
        app.scence_gui.signScence('main-game', main);
        
    } catch (e) {
        app.console.error('',e);
    }
};
