/*
~
~ 地图编辑器的ui数据
~
*/

;moudle = async function (self) {
const
dom = await require('lib/dommer'),
app = await require('var/app'),
NAME = await require('var/name'),
CONST = await require('var/const'),
BLOCKS = await require('script/BLOCKS'),
MODS = await require('script/MODS');
self.data = () => {return [{
    name: 'lane-gui-icon-close',
    args: [dom.width() - 96, 32, 64, 64, 'close'],
}]};
};
