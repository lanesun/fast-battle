/*
~
~ 地图编辑器的ui数据
~
*/

;moudle = async function (self) {
const
dom = await require('lib/dommer'),
app = await require('var/app'),
NAME = await require('var/name'),
CONST = await require('var/const'),
BLOCKS = await require('script/BLOCKS'),
MODS = await require('script/MODS');
self.data = () => {return [{
    name: 'lane-gui-icon-fullscreen',
    args: [dom.width() - 168, 32, 64, 64, 'change_fullscreen'],
    },{
    name: 'lane-gui-icon-save',
    args: [dom.width() - 240, 32, 64, 64, 'save_game_map'],
    },{
    name: 'lane-gui-icon-close',
    args: [dom.width() - 96, 32, 64, 64, 'close'],
    },{
    name: 'lane-gui-simple-frame',
    args: [0, 0, 390, dom.height()],
    },{
    name: 'lane-gui-icon-right-arrow',
    args: [288, 970, 64, 64, 'block_select_turn_right'],
    },{
    name: 'lane-gui-icon-left-arrow',
    args: [48, 970, 64, 64, 'block_select_turn_left'],
}]};
self.mapsize_box = () => {return [{
    name: 'lane-gui-text-box-center',
    args: [dom.width()/2 + 100,dom.height() - 24,NAME.WIDTH + ':' + app.gamemap.width,40,"#2e3436","#2e3436","pixelfont",'change_map_width'],
    },{
    name: 'lane-gui-text-box-center',
    args: [dom.width()/2 + 500,dom.height() - 24,NAME.HEIGHT + ':' + app.gamemap.height,40,"#2e3436","#2e3436","pixelfont",'change_map_height'],
}]};
self.selectblock_box = () => {return [{
    name: 'lane-gui-img-box',
    args: [BLOCKS.get(CONST.DEFAULT_BLOCK).prototype.img, 48,48,128,128],
}]};
self.position_popup = () => {return [{
    name: 'lane-gui-text',
    args: [0,0,'',40,"#000000","#000000","pixelfont"]
    },{
    name: 'lane-gui-text',
    args: [0,0,'',40,"#ffffff","#ffffff","pixelfont"]
}]};
};
