/*
~
~ 主页的ui数据
~
*/

;moudle = async function (self) {
const
dom = await require('lib/dommer'),
NAME = await require('var/name');
self.data = () => {return [{
name: 'lane-gui-text-center',
args: [dom.width()/2,dom.height()/2 + 250,NAME.HELP,40,"#909090","#ffffff","pixelfont",'help'],
},{
name: 'lane-gui-text-center',
args: [dom.width()/2,dom.height()/2 + 150,NAME.OPTIONS,40,"#909090","#ffffff","pixelfont",'options'],
},{
name: 'lane-gui-text',
args: [dom.width() - 300,dom.height() - 50,NAME.MAP_EDITOR,40,"#909090","#ffffff","pixelfont",'map_editor'],
},{
name: 'lane-gui-text-center',
args: [dom.width()/2,dom.height()/2,NAME.START_GAME,100,"#909090","#ffffff","pixelfont",'start_game'],
},{
name: 'lane-gui-text-center',
args: [dom.width()/2,dom.height()/2 - 300,NAME.$APP,150,"#729fcf","#729fcf","pixelfont"],
},{
name: 'lane-gui-text',
args: [dom.width()/2 + 300,dom.height()/2 - 250,NAME.$VERSION,60,"#fce94f","#fce94f","pixelfont"],
},{
name: 'lane-gui-icon-fullscreen',
args: [dom.width() - 168, 32, 64, 64, 'change_fullscreen'],
},{
name: 'lane-gui-icon-close',
args: [dom.width() - 96, 32, 64, 64, 'close'],
},];};
};
