;moudle = async function (self) {
    try {
        const
        dom = await require('lib/dommer'),
        wait = (await require('lib/timer')).wait,
        painter = await require('script/painter'),
        fade = await require('lib/fade'),
        calc = await require('lib/calc'),
        app = await require('var/app'),
        Gui = (await require('lib/gui')).Frame,
        FLAG = await require('var/flag'),
        fullscreen_helper = await require('script/fullscreen-helper'),
        IMGS = await require('script/IMGS'),
        NAME = await require('var/name'),
        MODS = await require('script/MODS'),
        data = (await require('scence/main-screen-data')).data();
        
        let addSomeStar = (data,n) => {
            for (let i = 0; i < n; i++) {
                let rad = Math.random();
                data.unshift({
                    name: 'lane-gui-star',
                    args: [dom.width() * Math.random(),dom.height() * Math.random(), rad * 16, rad * 16],
                });
            }
        };
        
        let changeFullscreen = () => {
            if (!fullscreen_helper.isFullscreen()) {
                fullscreen_helper.requestFullscreen();
            } else {
                fullscreen_helper.exitFullscreen();
            }
        };
        
        let drawer = (ctx) => {
            painter.fill(ctx, "#000");
        };
        
        let getIn = async () => {
            app.scence_gui.pushDrawer(drawer);
            app.scence_gui.pushDrawer(gui.drawer);
            await fade.in(app.scence_gui);
            app.scence_top.pushController(gui.controller);
        };
        
        let getOut = async () => {
            app.scence_top.exitController(gui.controller);
            await fade.out(app.scence_gui);
            app.scence_gui.exitDrawer(drawer);
            app.scence_gui.exitDrawer(gui.drawer);
        };
        
        let gui = null;
        
        let main = async () => {
            gui = new Gui();
            addSomeStar(data, 25);
            let elems = gui.loadData(data);
            gui.loadFn({
                change_fullscreen: changeFullscreen,
                close: async () => {
                    await getOut();
                    erequire('electron').remote.app.exit();
                },
                map_editor: async () => {
                    await getOut();
                    await app.scence_gui.pushScence('map-editor');
                    await getIn();
                },
                start_game: async () => {
                    await getOut();
                    await app.scence_gui.pushScence('main-game');
                    await getIn();
                },
            });
            await getIn();
        };
        
        app.scence_gui.signScence('main-screen', main);
        
    } catch (e) {
        app.console.error('',e);
    }
};
