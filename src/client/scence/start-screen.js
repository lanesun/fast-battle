;moudle = async function (self) {
    try {
        const
        dom = await require('lib/dommer'),
        wait = (await require('lib/timer')).wait,
        painter = await require('script/painter'),
        fade = await require('lib/fade'),
        app = await require('var/app'),
        GameMap = (await require('script/GameMap')).GameMap,
        FLAG = await require('var/flag'),
        IMGS = await require('script/IMGS'),
        NAME = await require('var/name'),
        CONST = await require('var/const'),
        MODS = await require('script/MODS');
        
        
        let drawer = (ctx) => {
            ctx.fillStyle = "#000";
            ctx.fillRect(0,0,ctx.canvas.width,ctx.canvas.height);
        };
        let main = async () => {
            dom.title(NAME.$APP);
            app.scence_gui.pushDrawer(drawer);
            await fade.in(app.scence_gui);
            app.console.printFn((res) => {
                if (IMGS.loaded >= IMGS.total()) {
                    res();
                }
                return NAME.LOADING_RESOURCE + ' ' + Math.floor(IMGS.loaded/IMGS.total()*100) + "%";
            })
            await IMGS.load();
            app.console.printFn((res) => {
                if (MODS.loaded >= MODS.total()) {
                    res();
                }
                return NAME.LOADING_MODS + ' ' + Math.floor(MODS.loaded/MODS.total()*100) + "%";
            })
            await MODS.load();
            await wait(1000);
            app.gamemap = new GameMap(20, 20);
            app.gamemap.fillBlocks(CONST.DEFAULT_BLOCK);
            app.console.print("------------------------------------------", 10000)
            app.console.print("         Fast Battle By Lane Sun          ", 10000);
            app.console.print("                  v" + NAME.$VERSION + "                  ", 10000, "#ff0");
            app.console.print("  Visit my Website to check out updates.  ", 10000);
            app.console.print("           https://anlbrain.com           ", 10000, "#09f");
            app.console.print("------------------------------------------", 10000);
            await fade.out(app.scence_gui);
            app.scence_gui.exitDrawer(drawer);
            await app.scence_gui.pushScence('main-screen');
        };
        
        app.scence_gui.signScence('start-screen', main);
        
    } catch (e) {
        app.console.error('',e);
    }
};
