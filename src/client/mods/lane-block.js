/*
~
~ 方块模组
~
*/

;moudle = async function (lane_block) {
    const IMGS = await require('script/IMGS'),
    Blocks = await require('script/BLOCKS'),
    object = await require('lib/object'),
    calc = await require('lib/calc'),
    app = await require('var/app'),
    gui = await require('lib/gui');
    
    const BLOCKS = (await require('mods/lane-block/BLOCKS')).BLOCKS;

    let objs = new Map();
    
    for (let id of BLOCKS) {
        objs.set(id, await require('mods/lane-block/'+ id));
    }
    
    lane_block.load = async () => {
        for (let [id, obj] of objs) {
            Blocks.signBlock(id, await obj.getClass());
        }
    }
    
};
