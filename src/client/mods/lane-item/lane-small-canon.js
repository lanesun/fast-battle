/*
~
~ 
~
*/

;moudle = async function (self) {
    const
    IMGS = await require('script/IMGS'),
    UNITS = await require('script/UNITS'),
    object = await require('lib/object'),
    CONST = await require('var/const'),
    NAME = await require('var/name'),
    Weapon = (await require('mods/lane-item/Weapon')).Weapon;
    
    self.getClass = async () => {
        return object.create({
            name: "SmallCanon",
            extend: Weapon,
            constructor: function (...args) {
                Weapon.call(this, ...args)
            },
            proto: {
                display_name: '小型加农炮',
                require_power: 1000,
                max_distance: 500,
                fire: (self, unit, target, gamemap) => {
                    unit.power = 0;
                    gamemap.units.push(new (UNITS.get('lane-small-bullet'))(unit.x, unit.y, unit.team, unit.face, target));
                }
            }
        });
    };
};
