/*
~
~ 步兵单位基类
~
*/

;moudle = async function (person) {
    const
    IMGS = await require('script/IMGS'),
    object = await require('lib/object'),
    Features = await require('mods/lane-unit/Features'),
    unit = await require('mods/lane-unit/unit');
    
    person.Person = object.create({
        name: "Person",
        extend: unit.Unit,
        extends: [
            Features.Hp,
            Features.Armed,
            Features.SimpleMove,
            Features.SimpleVisible,
            Features.RoundCollisible
        ],
        constructor: function (x,y) {
            Features.Hp.call(this);
            Features.Armed.call(this);
            Features.SimpleMove.call(this, x, y);
            Features.SimpleVisible.call(this);
            Features.RoundCollisible.call(this);
        },
        proto: {
            type: 'land',
            max_speed: 0,
            max_acceleration: 0,
            damp: 0,
            max_hp: 0,
            max_power: 0,
            animation_data: {
                stand: {
                    timeout: 0,
                    frames: [[],[]],
                    length: 0
                },
                walk: {
                    timeout: 0,
                    frames: [[],[]],
                    length: 0
                }
            },
            blood_frames: [],
            shadow_frame: null,
            collision_size: 0,
            logicH: (self, t) => {
                self.logicMove(self, t);
                self.logicPower(self, t);
                self.logicSimpleMove(self,t);
                self.logicRefreshAnimationFrame(self,t);
            },
        }
    });
};
