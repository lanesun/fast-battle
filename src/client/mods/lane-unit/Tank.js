/*
~
~ 步兵单位基类
~
*/

;moudle = async function (tank) {
    const
    IMGS = await require('script/IMGS'),
    object = await require('lib/object'),
    Features = await require('mods/lane-unit/Features'),
    unit = await require('mods/lane-unit/unit');
    
    tank.Tank = object.create({
        name: "Tank",
        extend: unit.Unit,
        extends: [
            Features.Hp,
            Features.Armed,
            Features.RotateMove,
            Features.RotateVisible,
            Features.RoundCollisible
        ],
        constructor: function (x,y) {
            Features.Hp.call(this);
            Features.Armed.call(this);
            Features.RotateMove.call(this, x, y);
            Features.RotateVisible.call(this);
            Features.RoundCollisible.call(this);
        },
        proto: {
            type: 'land',
            max_speed: 0,
            max_acceleration: 0,
            max_rotate_speed: 0,
            rotate_degree: 0,
            damp: 0,
            max_hp: 0,
            max_power: 0,
            animation_data: {
                default: null,
            },
            blood_frames: [],
            shadow_frame: null,
            collision_size: 0,
            logicH: (self, t) => {
                self.logicMove(self, t);
                self.logicPower(self, t);
                self.logicRotateMove(self,t);
            },
        }
    });
};
