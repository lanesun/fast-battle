/*
~
~ 子弹单位基类
~
*/

;moudle = async function (bullet) {
    const
    IMGS = await require('script/IMGS'),
    object = await require('lib/object'),
    Features = await require('mods/lane-unit/Features'),
    unit = await require('mods/lane-unit/unit');
    
    bullet.Bullet = object.create({
        name: "Bullet",
        extend: unit.Unit,
        extends: [
            Features.RotateMove,
            Features.RotateVisible
        ],
        constructor: function (x,y,team,face,target) {
            Features.RotateMove.call(this, x, y);
            Features.RotateVisible.call(this);
            this.face = face;
            this.team = team;
            this.target = target;
        },
        proto: {
            max_speed: 0,
            max_acceleration: 0,
            animation_data: {
                default: null,
            },
            length: null,
            logicCheck: (self, t) => {
                if (self.length <= 0) {
                    self.delete(self);
                }
            },
            logicTick: (self, t) => {},
            logicH: (self, t, gamemap) => {
                self.logicMove(self, t);
                self.logicRotateMove(self, t);
                self.logicTick(self, t, gamemap);
                self.logicCheck(self, t);
            },
            lowerDrawH: () => {}
        }
    });
};
