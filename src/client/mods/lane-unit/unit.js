/*
~
~ 单位的基类
~
*/

;moudle = async function (unit) {
    const
    object = await require('lib/object'),
    array = await require('lib/array'),
    eventer = await require('lib/eventer'),
    app = await require('var/app');
    unit.Unit = object.create({
        name: "Unit",
        extends: [eventer.EventTarget],
        constructor: function () {
            eventer.EventTarget.call(this);
        },
        proto: {
            visible: false,
            collisible: false,
            thinking: false,
            logical: false,
            type: null,
            delete: (self) => {
                self.deleted = true;
            },
        }
    });
};
