/*
~
~ 
~
*/

;moudle = async function (self) {
    const
    IMGS = await require('script/IMGS'),
    ITEMS = await require('script/ITEMS'),
    object = await require('lib/object'),
    calc = await require("lib/calc"),
    tank = await require('mods/lane-unit/Tank');
    IMGS.add('mods/lane-unit/tankB2.png');
    IMGS.add('mods/lane-unit/blood.png');
    IMGS.add('mods/lane-unit/shadow2.png');
    self.getClass = async () => {
        return object.create({
            name: 'Tank2',
            extend: tank.Tank,
            constructor: function (...args) {
                tank.Tank.call(this, ...args);
                this.weapon = new (ITEMS.get('lane-small-canon'));
            },
            proto: {
                display_name: '坦克2',
                max_speed: 0.2,
                max_acceleration: 0.001,
                max_rotate_speed: 0.1,
                rotate_degree: 0.5,
                damp: 0.0001,
                max_hp: 100,
                max_power: 1000,
                animation_data: {
                    default: await IMGS.get("mods/lane-unit/tankB2.png",0,0,48,48,48,48),
                },
                blood_frames: [
                    await IMGS.get("mods/lane-unit/blood.png",0,0,16,8,32,16),
                    await IMGS.get("mods/lane-unit/blood.png",0,8,16,8,32,16),
                    await IMGS.get("mods/lane-unit/blood.png",0,16,16,8,32,16),
                    await IMGS.get("mods/lane-unit/blood.png",0,24,16,8,32,16),
                    await IMGS.get("mods/lane-unit/blood.png",0,32,16,8,32,16),
                ],
                shadow_frame: await IMGS.get("mods/lane-unit/shadow2.png",0,0,32,32,25,25),
                collision_size: 18,
                thinking: true,
                ai: (self, t, gamemap) => {
                    let target = null;
                    for (let unit of gamemap.units) {
                        if (unit.team !== self.team && unit.hp > 0) {
                            if (!(target && calc.getDistance2(self, unit) > calc.getDistance2(self, target))) {
                                target = unit;
                            }
                        }
                    }
                    if (target) {
                        self.CMD_changeFace(self, Math.atan2(target.y - self.y, target.x - self.x));
                        if (calc.getDistance(self, target) > self.weapon.max_distance) {
                            self.CMD_speedTo(self, self.max_speed);
                        } else {
                            self.CMD_speedTo(self, 0);
                            if (Math.abs(calc.rollBetween(self.face - Math.PI, Math.atan2(target.y - self.y, target.x - self.x), self.face + Math.PI) - self.face) < 0.1)
                            self.CMD_fire(self, target, gamemap);
                        }
                    } else {
                        self.CMD_speedTo(self, 0);
                    }
                }
            }
        });
    }
};
