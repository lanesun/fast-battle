/*
~
~ 
~
*/

;moudle = async function (self) {
    const
    IMGS = await require('script/IMGS'),
    object = await require('lib/object'),
    person = await require('mods/lane-unit/Person');
    IMGS.add('mods/lane-unit/gun-man.png');
    IMGS.add('mods/lane-unit/blood.png');
    IMGS.add('mods/lane-unit/shadow.png');
    self.getClass = async () => {
        return object.create({
            name: 'GunMan',
            extend: person.Person,
            constructor: function (...args) {
                person.Person.call(this, ...args);
            },
            proto: {
                display_name: '男枪',
                max_speed: 0.2,
                max_acceleration: 0.001,
                damp: 0.0001,
                max_hp: 100,
                max_power: 500,
                animation_data: {
                    stand: {
                        timeout: 0,
                        frames: [[
                            await IMGS.get("mods/lane-unit/gun-man.png",0,0,32,32,80,80),
                        ],[
                            await IMGS.get("mods/lane-unit/gun-man.png",0,32,32,32,80,80),
                        ]],
                        length: 1
                    },
                    walk: {
                        timeout: 50,
                        frames: [[
                            await IMGS.get("mods/lane-unit/gun-man.png",0,0,32,32,80,80),
                            await IMGS.get("mods/lane-unit/gun-man.png",32,0,32,32,80,80),
                            await IMGS.get("mods/lane-unit/gun-man.png",64,0,32,32,80,80),
                            await IMGS.get("mods/lane-unit/gun-man.png",96,0,32,32,80,80),
                        ],[
                            await IMGS.get("mods/lane-unit/gun-man.png",0,32,32,32,80,80),
                            await IMGS.get("mods/lane-unit/gun-man.png",32,32,32,32,80,80),
                            await IMGS.get("mods/lane-unit/gun-man.png",64,32,32,32,80,80),
                            await IMGS.get("mods/lane-unit/gun-man.png",96,32,32,32,80,80),
                        ]],
                        length: 4
                    }
                },
                blood_frames: [
                    await IMGS.get("mods/lane-unit/blood.png",0,0,16,8,32,16),
                    await IMGS.get("mods/lane-unit/blood.png",0,8,16,8,32,16),
                    await IMGS.get("mods/lane-unit/blood.png",0,16,16,8,32,16),
                    await IMGS.get("mods/lane-unit/blood.png",0,24,16,8,32,16),
                    await IMGS.get("mods/lane-unit/blood.png",0,32,16,8,32,16),
                ],
                shadow_frame: await IMGS.get("mods/lane-unit/shadow.png",0,0,32,23,40,28),
                collision_size: 25
            }
        });
    }
};
