/*
~
~ 单位特性类
~
*/

;moudle = async function (FEATURES) {
const
object = await require('lib/object'),
calc = await require('lib/calc'),
collision = await require('lib/collision');
FEATURES.Move = object.create({
    name: "Move",
    constructor: function (x,y) {
        this.x = x;
        this.y = y;
        this.vx = 0;
        this.vy = 0;
        this.ax = 0;
        this.ay = 0;
        this.vself = 0;
        this.face = 0;
    },
    proto: {
        logical: true,
        max_speed: 0,
        max_acceleration: 0,
        damp: 0,
        logicMove: (self, t) => {
            if (self.length) {
                self.length -= self.vself * t;
            }
            self.x += self.vx * t + Math.cos(self.face) * self.vself * t;
            self.y += self.vy * t + Math.sin(self.face) * self.vself * t;
            self.vx = calc.closeTo(self.vx, 0, t * self.damp);
            self.vy = calc.closeTo(self.vy, 0, t * self.damp);
        },
        logicH: (self, t) => {
            self.logicMove(self, t);
        }
    }
});
FEATURES.Visible = object.create({
    name: "Visible",
    constructor: function () {},
    proto: {
        visible: true,
        drawH: null
    }
});
FEATURES.Hp = object.create({
    name: "Hp",
    constructor: function () {
        this.hp = this.max_hp;
    },
    proto: {
        max_hp: 0,
        getHurt: (self, hp) => {
            self.hp -= hp;
            if (self.hp <= 0) {
                self.delete(self);
            }
        }
    }
});
FEATURES.ArmoredHp = object.create({
    name: "ArmoredHp",
    extend: FEATURES.Hp,
    constructor: function () {
        FEATURES.Hp.call(this);
    },
    proto: {
        armor: 1,
        getHurt: (self, hp, strength) => {
            FEATURES.Hp.prototype.getHurt(self, strength / self.armor / (1 + Math.abs(strength - self.armor)) * hp);
        }
    }
});
FEATURES.Armed = object.create({
    name: "Armed",
    constructor: function () {
        this.power = 0;
        this.weapon = null;
    },
    proto: {
        max_power: 0,
        logical: true,
        CMD_fire: (self, ...args) => {
            if (self.power >= self.weapon.require_power) {
                return self.weapon.fire(self.weapon, self, ...args);
            } else {
                return false;
            }
        },
        logicPower: (self, t) => {
            self.power = calc.smallerThan(self.power + t, self.max_power);
        },
        logicH: (self, t) => {
            self.logicPower(self, t);
        }
    }
});
FEATURES.RotateMove = object.create({
    name: "RotateMove",
    extend: FEATURES.Move,
    constructor: function (x,y) {
        FEATURES.Move.call(this, x, y);
        this.target_speed = 0;
        this.target_face = 0;
        this.rotate_speed = 0;
    },
    proto: {
        max_rotate_speed: null,
        rotate_degree: null,
        CMD_changeFace: (self, face) => {
            self.target_face = calc.getAngle(face);
        },
        CMD_speedTo: (self, speed) => {
            self.target_speed = calc.inBetween(-self.max_speed, speed, self.max_speed);
        },
        logicRotateMove: (self, t) => {
            self.vself = calc.closeTo(self.vself, self.target_speed, t * self.max_acceleration);
            let facex = self.target_face - self.face;
            let agl = calc.flag(facex) * (Math.abs(facex) > Math.PI ? Math.abs(facex) - 2 * Math.PI : Math.abs(facex));
            self.rotate_speed = calc.inBetween(-self.max_rotate_speed, self.rotate_speed * (1 - self.rotate_degree) + agl * self.rotate_degree, self.max_rotate_speed);
            self.face = calc.getAngle(self.face + self.rotate_speed);
        },
        logicH: (self, t) => {
            self.logicRotateMove(self,t);
        }
    }
});
FEATURES.SimpleMove = object.create({
    name: "SimpleMove",
    extend: FEATURES.Move,
    constructor: function (x,y) {
        FEATURES.Move.call(this, x, y);
        this.target_speed = 0;
    },
    proto: {
        CMD_changeFace: (self, face) => {
            self.face = calc.getAngle(face);
        },
        CMD_speedTo: (self, speed) => {
            self.target_speed = calc.inBetween(-self.max_speed, speed, self.max_speed);
        },
        logicSimpleMove: (self, t) => {
            self.vself = calc.closeTo(self.vself, self.target_speed, t * self.max_acceleration);
        },
        logicH: (self, t) => {
            self.logicSimpleMove(self,t);
        }
    }
});
FEATURES.RotateVisible = object.create({
    name: 'RotateVisible',
    extend: FEATURES.Visible,
    constructor: function () {
        FEATURES.Visible.call(this);
    },
    proto: {
        animation_data: {
            default: null,
        },
        blood_frames: [],
        shadow_frame: null,
        drawH: (self, ctx) => {
            ctx.drawImage(self.animation_data.default, self.x, self.y, 0, self.animation_data.default.height/2,{
                rotate: self.face + Math.PI/2,
            });
            if (self.hp < self.max_hp) {
                ctx.drawImage(self.blood_frames[calc.inBetween(0, Math.round((self.max_hp - self.hp)/self.max_hp * 4), 4)], self.x, self.y, 0, -self.animation_data.default.height/2);
            }
        },
        lowerDrawH: (self, ctx) => {
            ctx.drawImage(self.shadow_frame, self.x, self.y, 0, self.shadow_frame.height/2);
        }
    }
});
FEATURES.SimpleVisible = object.create({
    name: "SimpleVisible",
    extend: FEATURES.Visible,
    constructor: function () {
        FEATURES.Visible.call(this);
        this.animation = 0;
        this.old_animation_status = 'stand';
        this.animation_time = 0;
    },
    proto: {
        animation_data: {
            stand: {
                timeout: 0,
                frames: [[],[]],
                length: 0
            },
            walk: {
                timeout: 0,
                frames: [[],[]],
                length: 0
            }
        },
        blood_frames: [],
        shadow_frame: null,
        getAnimationStatus: (self) => {
            if (self.vself !== 0) {
                return 'walk';
            }
            return 'stand';
        },
        getAnimationFrame: (self) => {
            if (calc.rollBetween(-Math.PI, self.face + Math.PI/4*3, Math.PI) > 0) {
                return self.animation_data[self.getAnimationStatus(self)].frames[0][self.animation];
            } else {
                return self.animation_data[self.getAnimationStatus(self)].frames[1][self.animation];
            }
        },
        logicRefreshAnimationFrame: (self, t) => {
            let astatus = self.getAnimationStatus(self);
            if (astatus !== self.old_animation_status) {
                self.animation = 0;
                self.animation_time = 0;
                self.old_animation_status = astatus;
            } else {
                self.animation_time += t;
                if (self.animation_time >= self.animation_data[astatus].timeout) {
                    self.animation = calc.rollBetween(-1, self.animation + 1, self.animation_data[astatus].length - 1);
                    self.animation_time = 0;
                }
            }
        },
        logicH: (self, t) => {
            self.logicRefreshAnimationFrame(self,t);
        },
        drawH: (self, ctx) => {
            let img = self.getAnimationFrame(self);
            ctx.drawImage(img, self.x, self.y);
            if (self.hp < self.max_hp) {
                ctx.drawImage(self.blood_frames[Math.round(self.hp/self.max_hp * 4)], self.x, self.y, 0, -img.height);
            }
        },
        lowerDrawH: (self, ctx) => {
            ctx.drawImage(self.shadow_frame, self.x, self.y, 0, self.shadow_frame.height/2 - 4);
        }
    }
});
FEATURES.RoundCollisible = object.create({
    name: "RoundCollisible",
    constructor: function () {},
    proto: {
        collisible: true,
        collision_size: 0,
        collision_type: 'round',
        collisionH: (self, target) => {
            switch (target.collision_type) {
                case 'round':
                    collision.rdrdCollisionSoft(self, target);
                    break;
                case 'rect':
                    collision.rdrtCollision(self, target);
                    break;
            }
        }
    }
});
FEATURES.RectCollisible = object.create({
    name: "RectCollisible",
    constructor: function () {},
    proto: {
        collisible: true,
        collision_width: 0,
        collision_height: 0,
        collision_type: 'rect',
        collisionH: (self, target) => {
            switch (target.collision_type) {
                case 'round':
                    collision.rdrtCollision(target, self);
                    break;
                case 'rect':
                    collision.rtrtCollision(self, target);
                    break;
            }
        }
    }
});
};
