/*
~
~ 
~
*/

;moudle = async function (self) {
    const
    IMGS = await require('script/IMGS'),
    object = await require('lib/object'),
    calc = await require('lib/calc'),
    Bullet = (await require('mods/lane-unit/Bullet')).Bullet;
    IMGS.add('mods/lane-unit/bullet_lightfire.png');
    self.getClass = async () => {
        return object.create({
            name: 'SmallBullet',
            extend: Bullet,
            constructor: function (...args) {
                Bullet.call(this, ...args);
            },
            proto: {
                display_name: '小炮弹',
                hurt: 10,
                max_speed: 0.6,
                max_acceleration: 0.01,
                max_rotate_speed: 0.01,
                rotate_degree: 0.5,
                animation_data: {
                    default: await IMGS.get("mods/lane-unit/bullet_lightfire.png",0,0,48,48,48,48),
                },
                length: 500,
                logicTick: (self, t, gamemap) => {
                    self.CMD_speedTo(self, self.max_speed);
                    for (let unit of gamemap.units) {
                        if (unit.team !== self.team && unit.hp > 0 && unit.collision_size) {
                            if (calc.getDistance(unit, self) <= unit.collision_size) {
                                unit.getHurt(unit, self.hurt);
                                self.delete(self);
                                return;
                            }
                        }
                    }
                    if (self.target.deleted) return;
                    self.CMD_changeFace(self, Math.atan2(self.target.y - self.y, self.target.x - self.x));
                    let r = self.target.collision_size ? self.target.collision_size : 1;
                    if (calc.getDistance(self.target, self) <= r) {
                        self.target.getHurt(self.target, self.hurt);
                        self.delete(self);
                    }
                }
            }
        });
    }
};
