;moudle = async function (self) {
const IMGS = await require('script/IMGS'),
object = await require('lib/object'),
CONST = await require('var/const'),
NAME = await require('var/name'),
Block = (await require('mods/lane-block/Block')).obj;
IMGS.add('mods/lane-block/MntCliff.png');

self.getClass = async () => {
return object.create({
name: "Land1tc",
extend: Block,
constructor: function (...args) {
Block.call(this, ...args)
},
proto: {
display_name: '地面1',
allowType: new Set(["land","land&water","air"]),
img: await IMGS.get('mods/lane-block/MntCliff.png',20,0,20,20,1 * CONST.BLOCK_DISPLAY_SIZE,1 * CONST.BLOCK_DISPLAY_SIZE)
}
});
};
};
