;moudle = async function (self) {
const IMGS = await require('script/IMGS'),
object = await require('lib/object'),
CONST = await require('var/const'),
NAME = await require('var/name'),
Block = (await require('mods/lane-block/Block')).obj;
IMGS.add('mods/lane-block/Mnt2Lava.png');

self.getClass = async () => {
return object.create({
name: "Lava1br",
extend: Block,
constructor: function (...args) {
Block.call(this, ...args)
},
proto: {
display_name: '岩浆1',
allowType: new Set(["air"]),
img: await IMGS.get('mods/lane-block/Mnt2Lava.png',40,40,20,20,1 * CONST.BLOCK_DISPLAY_SIZE,1 * CONST.BLOCK_DISPLAY_SIZE)
}
});
};
};
