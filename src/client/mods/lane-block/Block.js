/*
~
~ 
~
*/

let doCollision = (x, y, w, h, target) => {
    let disX = w/2 - Math.abs(target.x - x);
    let disY = h/2 - Math.abs(target.y - y);
    if (Math.abs(disX) < Math.abs(disY)) {
        if (!target.locked) target.x += target.x > x ? disX : -disX;
    } else {
        if (!target.locked) target.y += target.y > y ? disY : -disY;
    }
};

;moudle = async function (self) {
    const
    calc = await require("lib/calc"),
    CONST = await require('var/const'),
    object = await require('lib/object');
    self.obj = object.create({
        name: "Block",
        constructor: function (x, y) {
            this.x = x;
            this.y = y;
        },
        proto: {
            display_name: null,
            img: null,
            allowType: new Set([]),//[land|water|air|land&water]
            drawH: (self, s2p, gamemap) => {
                s2p.drawImage(self.img, self.x, self.y, 0, gamemap.block_disply_size/2);
            },
            collisionH: (self, target, gamemap) => {
                if (!self.allowType.has(target.type)) {
                    doCollision(self.x, self.y, gamemap.block_collosion_size, gamemap.block_collosion_size, target);
                    if (gamemap.getBlock(calc.floorToBlock(self.x) + 1, calc.floorToBlock(self.y)) && !gamemap.getBlock(calc.floorToBlock(self.x) + 1, calc.floorToBlock(self.y)).allowType.has(target.type)) {
                        doCollision(self.x + gamemap.block_real_size/2, self.y, gamemap.block_collosion_size * 2, gamemap.block_collosion_size, target);
                    }
                    if (gamemap.getBlock(calc.floorToBlock(self.x) - 1, calc.floorToBlock(self.y)) && !gamemap.getBlock(calc.floorToBlock(self.x) - 1, calc.floorToBlock(self.y)).allowType.has(target.type)) {
                        doCollision(self.x - gamemap.block_real_size/2, self.y, gamemap.block_collosion_size * 2, gamemap.block_collosion_size, target);
                    }
                    if (gamemap.getBlock(calc.floorToBlock(self.x), calc.floorToBlock(self.y) + 1) && !gamemap.getBlock(calc.floorToBlock(self.x), calc.floorToBlock(self.y) + 1).allowType.has(target.type)) {
                        doCollision(self.x, self.y + gamemap.block_real_size/2, gamemap.block_collosion_size, gamemap.block_collosion_size * 2, target);
                    }
                    if (gamemap.getBlock(calc.floorToBlock(self.x), calc.floorToBlock(self.y) - 1) && !gamemap.getBlock(calc.floorToBlock(self.x), calc.floorToBlock(self.y) - 1).allowType.has(target.type)) {
                        doCollision(self.x, self.y - gamemap.block_real_size/2, gamemap.block_collosion_size, gamemap.block_collosion_size * 2, target);
                    }
                }
            }
        }
    });
};
