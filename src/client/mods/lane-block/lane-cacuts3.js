;moudle = async function (self) {
const IMGS = await require('script/IMGS'),
object = await require('lib/object'),
CONST = await require('var/const'),
NAME = await require('var/name'),
Block = (await require('mods/lane-block/Block')).obj;
IMGS.add('mods/lane-block/MntMisc.png');

self.getClass = async () => {
return object.create({
name: "Cactus1",
extend: Block,
constructor: function (...args) {
Block.call(this, ...args)
},
proto: {
display_name: '仙人掌3',
allowType: new Set(["air"]),
img: await IMGS.get('mods/lane-block/MntMisc.png',40,0,20,40,1 * CONST.BLOCK_DISPLAY_SIZE,2 * CONST.BLOCK_DISPLAY_SIZE)
}
});
};
};
