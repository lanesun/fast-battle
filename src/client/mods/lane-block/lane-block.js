/*
~
~ 
~
*/

;moudle = async function (self) {
    const IMGS = await require('script/IMGS'),
    object = await require('lib/object'),
    CONST = await require('var/const'),
    NAME = await require('var/name'),
    Block = (await require('mods/lane-block/Block')).obj;
    IMGS.add('mods/lane-block/lane-block.png');
    
    self.getClass = async () => {
        return object.create({
            name: "Block",
            extend: Block,
            constructor: function (...args) {
                Block.call(this, ...args)
            },
            proto: {
                display_name: '阻隔',
                img: await IMGS.get('mods/lane-block/lane-block.png', 0,0,32,32, CONST.BLOCK_DISPLAY_SIZE, CONST.BLOCK_DISPLAY_SIZE),
                allowType: new Set([]),
                drawH: () => {}
            }
        });
    };
};
