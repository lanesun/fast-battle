/*
~
~ 单位模组
~
*/

;moudle = async function (lane_unit) {
    const
    Units = await require('script/UNITS'),
    object = await require('lib/object');
    
    const UNITS = (await require('mods/lane-unit/UNITS')).UNITS;

    let objs = new Map();
    
    for (let id of UNITS) {
        objs.set(id, await require('mods/lane-unit/'+ id));
    }
    
    lane_unit.load = async () => {
        for (let [id, obj] of objs) {
            Units.signUnit(id, await obj.getClass());
        }
    }
    
};
