/*
~
~ 物品模组
~
*/

;moudle = async function (lane_item) {
    const
    Items = await require('script/ITEMS'),
    object = await require('lib/object');
    
    const ITEMS = (await require('mods/lane-item/ITEMS')).ITEMS;

    let objs = new Map();
    
    for (let id of ITEMS) {
        objs.set(id, await require('mods/lane-item/'+ id));
    }
    
    lane_item.load = async () => {
        for (let [id, obj] of objs) {
            Items.signItem(id, await obj.getClass());
        }
    }
    
};
