/*
~
~ Lane Sun 自用的UI仓库
~ 注意，是“自用”！！
~
*/

const IMG_SIZE = 48;
const FONT = 'pixelfont';
const BC = '#000';
const FC = '#656564';
const SHADOW_MX = 2;
const SHADOW_MY = 2;

;moudle = async (lane_gui) => {
    const IMGS = await require('script/IMGS'),
    painter = await require('script/painter'),
    object = await require('lib/object'),
    calc = await require('lib/calc'),
    app = await require('var/app'),
    dom = await require('lib/dommer'),
    gui = await require('lib/gui');
    const gui_src = 'mods/lane-gui/gui.png';
    IMGS.add(gui_src);
    IMGS.add('mods/lane-gui/earth.png');
    
    let getTextWidth = (size, str, font = FONT) => {
        app.scence.context.font = size + "px " + font;
        return app.scence.context.measureText(str).width;
    }
    
    lane_gui.load = async () => {
        let frames_img = [];
        for (let i = 0; i < 6; i ++) {
            frames_img[i] = [];
            for (let j = 0; j < 6; j ++) {
                frames_img[i][j] = await IMGS.get(gui_src, i * 8, j * 8, 8, 8, IMG_SIZE, IMG_SIZE);
            }
        }
        let popframes_img = [];
        for (let i = 0; i < 3; i ++) {
            popframes_img[i] = [];
            for (let j = 0; j < 3; j ++) {
                popframes_img[i][j] = await IMGS.get(gui_src, 96 + i * 8, 24 + j * 8, 8, 8, IMG_SIZE, IMG_SIZE);
            }
        }
        let img_map = {
            defa: {
                color: '#ffffff',
                map: [0,0]
            },
            hovered: {
                color: '#fdff79',
                map: [3,0]
            },
            pushed: {
                color: '#848484',
                map: [0,3]
            }
        };
        
        let getMap = (self) => {
            if (self.hovered) {
                if (self.pushed) {
                    return img_map.pushed;
                }
                return img_map.hovered;
            }
            return img_map.defa;
        }
        
        let SimpleBox = object.create({
            name: "SimpleBox",
            constructor: function (x, y, width, height, handle) {
                this.x = x;
                this.y = y;
                this.width = width;
                this.height = height;
                this.hovered = false;
                this.pushed = false;
                this.handle = handle;
            },
            proto: {
                collisible: true,
                getX: (self) => self.x,
                getY: (self) => self.y,
                getW: (self) => self.width,
                getH: (self) => self.height,
                getBoxX: (self) => self.x + self.width/2,
                getBoxY: (self) => self.y + self.height/2,
                getBoxW: (self) => self.width + IMG_SIZE,
                getBoxH: (self) => self.height + IMG_SIZE,
                drawH: (self, ctx) => {
                    let x = self.getX(self),
                    y = self.getY(self),
                    w = self.getW(self),
                    h = self.getH(self);
                    let map = getMap(self);
                    ctx.drawImage(frames_img[map.map[0] + 0][map.map[1] + 0], x - IMG_SIZE, y - IMG_SIZE);
                    ctx.drawImage(frames_img[map.map[0] + 1][map.map[1] + 0], x, y - IMG_SIZE, w, IMG_SIZE);
                    ctx.drawImage(frames_img[map.map[0] + 2][map.map[1] + 0], x + w, y - IMG_SIZE);
                    ctx.drawImage(frames_img[map.map[0] + 2][map.map[1] + 1], x + w, y, IMG_SIZE, h);
                    ctx.drawImage(frames_img[map.map[0] + 2][map.map[1] + 2], x + w, y + h);
                    ctx.drawImage(frames_img[map.map[0] + 1][map.map[1] + 2], x, y + h, w, IMG_SIZE);
                    ctx.drawImage(frames_img[map.map[0] + 0][map.map[1] + 2], x - IMG_SIZE, y + h);
                    ctx.drawImage(frames_img[map.map[0] + 0][map.map[1] + 1], x - IMG_SIZE, y, IMG_SIZE, h);
                    ctx.fillStyle = map.color;
                    ctx.fillRect(x - 1,y - 1,w + 2,h + 2);
                },
                onmousein: (self, e) => {
                    self.hovered = true;
                },
                onmouseout: (self, e) => {
                    self.hovered = false;
                    self.pushed = false;
                },
                onmousedown: (self, e) => {
                    self.hovered = true;
                    self.pushed = true;
                    if (self.handle) {                
                        self.gui.callFn(self.handle, self);
                    }
                },
                onmouseup: (self, e) => {
                    self.hovered = true;
                    self.pushed = false;
                },
                onmousemove: (self, e) => {
                    self.hovered = true;
                },
            }
        });
        let SimpleFrame = object.create({
            name: 'SimpleFrame',
            extend: SimpleBox,
            constructor: function (...args) {
                SimpleBox.call(this, ...args);
                this.pushed = true;
                this.hovered = true;
            },
            proto: {
                collisible: false,
            }
        });
        let Popup = object.create({
            name: "Popup",
            extend: SimpleBox,
            constructor: function (...args) {
                SimpleBox.call(this, ...args);
            },
            proto: {
                drawH: (self, ctx) => {
                    let x = self.getX(self),
                    y = self.getY(self),
                    w = self.getW(self),
                    h = self.getH(self);
                    let map = getMap(self);
                    ctx.drawImage(popframes_img[0][0], x - IMG_SIZE, y - IMG_SIZE);
                    ctx.drawImage(popframes_img[1][0], x, y - IMG_SIZE, w, IMG_SIZE);
                    ctx.drawImage(popframes_img[2][0], x + w, y - IMG_SIZE);
                    ctx.drawImage(popframes_img[2][1], x + w, y, IMG_SIZE, h);
                    ctx.drawImage(popframes_img[2][2], x + w, y + h);
                    ctx.drawImage(popframes_img[1][2], x, y + h, w, IMG_SIZE);
                    ctx.drawImage(popframes_img[0][2], x - IMG_SIZE, y + h);
                    ctx.drawImage(popframes_img[0][1], x - IMG_SIZE, y, IMG_SIZE, h);
                    ctx.fillStyle = "#fff";
                    ctx.fillRect(x - 1,y - 1,w + 2,h + 2);
                },
            }
        });
        let Text = object.create({
            name: "Text",
            extend: SimpleBox,
            constructor: function (x, y, str, size, color = FC, hovercolor = FC, font = FONT, handle) {
                this.str = str;
                this.size = size;
                this.color = color;
                this.hcolor = hovercolor;
                this.font = font;
                this.x = x;
                this.y = y;
                this.width = getTextWidth(size, str, font);
                this.height = size;
                this.handle = handle;
            },
            proto: {
                getX: (self) => self.x,
                getY: (self) => self.y,
                getW: (self) => self.width,
                getH: (self) => self.height,
                getBoxW: (self) => self.width,
                getBoxH: (self) => self.height,
                drawH: (self, ctx) => {
                    let x = self.getX(self),
                    y = self.getY(self);
                    ctx.font = self.size + "px " + self.font;
                    ctx.fillStyle = self.hovered ? self.hcolor : self.color;
                    ctx.fillText(self.str, x, y + self.size/4*3);
                },
                rewidth: (self) => {
                    self.width = getTextWidth(self.size, self.str, self.font)
                }
            }
        });
        let TextBox = object.create({
            name: "TextBox",
            extend: SimpleBox,
            constructor: function (x, y, str, size, color = FC, hovercolor = FC, font = FONT, handle) {
                SimpleBox.call(this, x, y, getTextWidth(size, str, font), size);
                this.str = str;
                this.size = size;
                this.color = color;
                this.hcolor = hovercolor;
                this.font = font;
                this.handle = handle;
            },
            proto: {
                drawH: (self, ctx) => {
                    SimpleBox.prototype.drawH(self,ctx);
                    Text.prototype.drawH(self,ctx);
                },
                rewidth: (self) => {
                    self.width = getTextWidth(self.size, self.str, self.font)
                }
            }
        });
        let ImgBox = object.create({
            name: "ImgBox",
            extend: SimpleBox,
            constructor: function (img, x, y, w, h, handle) {
                SimpleBox.call(this, x, y, w, h, handle);
                this.img = img;
            },
            proto: {
                drawH: (self, ctx) => {
                    SimpleBox.prototype.drawH(self,ctx);
                    ctx.drawImage(self.img, self.getX(self), self.getY(self), self.getW(self), self.getH(self));
                },
            }
        });
        let ImgBoxPopup = object.create({
            name: "ImgBox",
            extend: SimpleBox,
            constructor: function (img, x, y, w, h, handle, data, str, size, color = FC, font = FONT) {
                SimpleBox.call(this, x, y, w, h, handle);
                this.str = str;
                this.popup = new TextPopup(x + w + IMG_SIZE, y, str, size, color, color, font);
                this.data = data;
                this.img = img;
            },
            proto: {
                drawH: (self, ctx) => {
                    SimpleBox.prototype.drawH(self,ctx);
                    ctx.drawImage(self.img, self.getX(self), self.getY(self), self.getW(self), self.getH(self));
                    if (self.hovered) {
                        TextPopup.prototype.drawH(self.popup, ctx);
                    }
                },
                onmousedown: (self, e) => {
                    self.hovered = true;
                    self.pushed = true;
                    if (self.handle) {                
                        self.gui.callFn(self.handle, self, self.data);
                    }
                }
            }
        });
        let TextPopup = object.create({
            name: "TextPopup",
            extend: Popup,
            constructor: function (x, y, str, size, color = FC, hovercolor = FC, font = FONT, handle) {
                Popup.call(this, x, y, getTextWidth(size, str, font), size);
                this.str = str;
                this.size = size;
                this.color = color;
                this.hcolor = hovercolor;
                this.font = font;
                this.handle = handle;
            },
            proto: {
                drawH: (self, ctx) => {
                    Popup.prototype.drawH(self,ctx);
                    Text.prototype.drawH(self,ctx);
                },
                rewidth: (self) => {
                    self.width = getTextWidth(self.size, self.str, self.font)
                }
            }
        });
        let Center = object.create({
            name: "Center",
            constructor: function () {},
            proto: {
                getX: (self) => self.x - self.width/2,
                getY: (self) => self.y - self.height/2,
                getW: (self) => self.width,
                getH: (self) => self.height,
                getBoxX: (self) => self.x,
                getBoxY: (self) => self.y,
                getBoxW: (self) => self.width + IMG_SIZE,
                getBoxH: (self) => self.height + IMG_SIZE,
            }
        });
        let TextCenter = object.create({
            name: "TextCenter",
            extend: Text,
            extends: [Center],
            constructor: function (...args) {
                Text.call(this, ...args);
            }
        });
        let TextBoxCenter = object.create({
            name: "TextBoxCenter",
            extend: TextBox,
            extends: [Center],
            constructor: function (...args) {
                TextBox.call(this, ...args);
            }
        });
        let PopupCenter = object.create({
            name: "PopupCenter",
            extend: Popup,
            extends: [Center],
            constructor: function (...args) {
                Popup.call(this, ...args);
            }
        });
        let Icon = object.create({
            name: "Icon",
            extend: SimpleBox,
            constructor: function (...args) {
                SimpleBox.call(this, ...args);
            },
            proto: {
                img: null,
                himg: null,
                getX: (self) => self.x,
                getY: (self) => self.y,
                getW: (self) => self.width,
                getH: (self) => self.height,
                getBoxW: (self) => self.width,
                getBoxH: (self) => self.height,
                drawH: (self, ctx) => {
                    let img = self.hovered ? self.himg : self.img;
                    ctx.drawImage(img, self.getX(self), self.getY(self), self.getW(self), self.getH(self));
                }
            }
        });
        let IconFullscreen = object.create({
            name: "IconFullscreen",
            extend: Icon,
            constructor: function (...args) {
                Icon.call(this, ...args);
            },
            proto: {
                img: await IMGS.get(gui_src, 136, 0, 16, 16, 16, 16),
                himg: await IMGS.get(gui_src, 136, 16, 16, 16, 16, 16)
            }
        });
        let IconSave = object.create({
            name: "IconSave",
            extend: Icon,
            constructor: function (...args) {
                Icon.call(this, ...args);
            },
            proto: {
                img: await IMGS.get(gui_src, 152, 0, 16, 16, 16, 16),
                himg: await IMGS.get(gui_src, 152, 16, 16, 16, 16, 16)
            }
        });
        let IconClose = object.create({
            name: "IconClose",
            extend: Icon,
            constructor: function (...args) {
                Icon.call(this, ...args);
            },
            proto: {
                img: await IMGS.get(gui_src, 48, 16, 16, 16, 16, 16),
                himg: await IMGS.get(gui_src, 64, 16, 16, 16, 16, 16)
            }
        });
        let IconRightArrow = object.create({
            name: "IconRightArrow",
            extend: Icon,
            constructor: function (...args) {
                Icon.call(this, ...args);
            },
            proto: {
                img: await IMGS.get(gui_src, 184, 0, 16, 16, 16, 16),
                himg: await IMGS.get(gui_src, 184, 16, 16, 16, 16, 16)
            }
        });
        let IconLeftArrow = object.create({
            name: "IconLeftArrow",
            extend: Icon,
            constructor: function (...args) {
                Icon.call(this, ...args);
            },
            proto: {
                img: await IMGS.get(gui_src, 200, 0, 16, 16, 16, 16),
                himg: await IMGS.get(gui_src, 200, 16, 16, 16, 16, 16)
            }
        });
        let Star = object.create({
            name: "Star",
            extend: Icon,
            constructor: function (...args) {
                Icon.call(this, ...args);
            },
            proto: {
                collisible: false,
                img: await IMGS.get(gui_src, 136, 32, 16, 16, 16, 16)
            }
        });
        let FloatEarth = object.create({
            name: "FloatEarth",
            extend: Icon,
            constructor: function (...args) {
                Icon.call(this, ...args);
                this.agl = 0;
            },
            proto: {
                collisible: false,
                img: await IMGS.get('mods/lane-gui/earth.png', 0, 0, 128, 128, 128, 128),
                getY: (self) => self.y + Math.sin(self.agl) * 50,
                drawH: (self, ctx, t) => {
                    self.agl = calc.rollBetween(0, self.agl + t/1000, 2*Math.PI);
                    Icon.prototype.drawH(self,ctx);
                }
            }
        });
        
        gui.signElem('lane-gui-simple-box', SimpleBox);
        gui.signElem('lane-gui-simple-frame', SimpleFrame);
        gui.signElem('lane-gui-popup', Popup);
        gui.signElem('lane-gui-text-box', TextBox);
        gui.signElem('lane-gui-img-box', ImgBox);
        gui.signElem('lane-gui-img-box-popup', ImgBoxPopup);
        gui.signElem('lane-gui-text-popup', TextPopup);
        gui.signElem('lane-gui-text', Text);
        gui.signElem('lane-gui-text-box-center', TextBoxCenter);
        gui.signElem('lane-gui-text-center', TextCenter);
        gui.signElem('lane-gui-popup-center', PopupCenter);
        gui.signElem('lane-gui-icon-fullscreen', IconFullscreen);
        gui.signElem('lane-gui-icon-save', IconSave);
        gui.signElem('lane-gui-icon-close', IconClose);
        gui.signElem('lane-gui-icon-right-arrow', IconRightArrow);
        gui.signElem('lane-gui-icon-left-arrow', IconLeftArrow);
        gui.signElem('lane-gui-star', Star);
        gui.signElem('lane-gui-float-earth', FloatEarth);
    };
    lane_gui.prompt = (gui, scence, message, text, x, y, size = 32, font = FONT) => {
        return new Promise((resolve) => {
            if (app.typing) {
                resolve();
                return;
            }
            app.typing = true;
            let str = text.toString();
            let popup = gui.loadData([{
                name: 'lane-gui-popup-center',
                args: [x, y + size - 8, Math.max(getTextWidth(size, str + '_', font),getTextWidth(size, message, font)) + size, 2 * size + 24],
            },{
                name: 'lane-gui-text-center',
                args: [x, y + size + 12, text + '_', size, '#000', '#000', font],
            },{
                name: 'lane-gui-text-center',
                args: [x, y, message, size, '#000', '#000', font],
            }]);
            popup[1].width = Math.max(getTextWidth(size, str + '_', font),getTextWidth(size, message, font)) + size;
            let controller = {
                keydown: (e) => {
                    if (e.key === 'Enter') {
                        scence.exitController(controller);
                        gui.deleteElem(...popup);
                        app.typing = false;
                        resolve(str);
                    } else if (e.key === 'Escape') {
                        scence.exitController(controller);
                        gui.deleteElem(...popup);
                        app.typing = false;
                        resolve(false);
                    } else if (e.key === 'Backspace') {
                        str = str.slice(0, str.length - 1);
                        popup[1].str = str + '_';
                        popup[0].width = popup[1].width = Math.max(getTextWidth(size, str + '_', font),getTextWidth(size, message, font)) + size;
                    } else if (e.key.length === 1) {
                        str += e.key;
                        popup[1].str = str + '_';
                        popup[0].width = popup[1].width = Math.max(getTextWidth(size, str + '_', font),getTextWidth(size, message, font)) + size;
                    }
                    return false;
                }
            };
            scence.pushController(controller);
        });
    };
    lane_gui.getStr = (gui, scence, text, x, y, size = 32, font = FONT) => {
        return new Promise((resolve) => {
            if (app.typing) {
                resolve();
                return;
            }
            app.typing = true;
            let str = text.toString();
            let popup = gui.loadData([{
                name: 'lane-gui-popup-center',
                args: [x, y + size + 12, getTextWidth(size, str + '_', font) + size/2, size + 12],
            },{
                name: 'lane-gui-text-center',
                args: [x, y + size + 12, text + '_', size, '#000', '#000', font],
            }]);
            popup[1].width = getTextWidth(size, str + '_', font) + size/2;
            let controller = {
                keydown: (e) => {
                    if (e.key === 'Enter') {
                        scence.exitController(controller);
                        gui.deleteElem(...popup);
                        app.typing = false;
                        resolve(str);
                    } else if (e.key === 'Escape') {
                        scence.exitController(controller);
                        gui.deleteElem(...popup);
                        app.typing = false;
                        resolve(false);
                    } else if (e.key === 'Backspace') {
                        str = str.slice(0, str.length - 1);
                        popup[1].str = str + '_';
                        popup[0].width = popup[1].width = getTextWidth(size, str + '_', font) + size/2;
                    } else if (e.key.length === 1) {
                        str += e.key;
                        popup[1].str = str + '_';
                        popup[0].width = popup[1].width = getTextWidth(size, str + '_', font) + size/2;
                    }
                    return false;
                }
            };
            scence.pushController(controller);
        });
    }
}
