#!/bin/sh

echo ";moudle = function (modlist) {
modlist.data = [" > ../var/modlist.js

for filename in `ls | grep '.*.js'`
do
    echo "'${filename%.*}',"  >> ../var/modlist.js
done

echo "];};" >> ../var/modlist.js

for dir in `ls -1 -d */`
do
    cd $dir
    sh generate.sh
    cd ../
done
