/*
~
~ 计算工具库
~
*/

;moudle = async function (calc) {
    const CONST = await require('var/const');
    const NU_1S2 = 1/(2**(0.5));
    const NU_S2 = 2**(0.5);
    calc.transX = (x,y) => x;
    calc.transY = (x,y) => y;
    calc.transScreenX = (s2painter, x, y) => calc.transX(x - s2painter.canvas.width/2, y - s2painter.canvas.height/2) + s2painter.position.x;
    calc.transScreenY = (s2painter, x, y) => calc.transY(x - s2painter.canvas.width/2, y - s2painter.canvas.height/2) + s2painter.position.y;
    calc.getDistance = (a, b) => calc.getDistance2(a,b)**0.5;
    calc.getDistance2 = (a, b) => (a.x - b.x)**2 + (a.y - b.y)**2;
    calc.getAngle = (agl) => calc.rollBetween(0, agl, 2 * Math.PI);
    calc.inBetween = (a, b, c) => (b > c ? c : b < a ? a : b);
    calc.smallerThan = (a, b) => (a > b ? b : a);
    calc.largerThan = (a, b) => (a < b ? b : a);
    calc.rollBetween = (a, b, c) => (b > c ? b - c + a : b < a ? b - a + c : b);
    calc.closeTo = (a, b, n) => (a > b ? calc.largerThan(a - n, b) : calc.smallerThan(a + n, b));
    calc.absPlus = (a, b) => closeTo(a, 0, -n);
    calc.floorToBlock = (a) => Math.floor(a/CONST.BLOCK_REAL_SIZE + 0.5);
};
