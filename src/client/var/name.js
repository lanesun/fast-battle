;moudle = function (NAME) {

    NAME.$APP = "Fast Battle";
    NAME.$VERSION = "2.1.2";
    NAME.START_GAME = "Start Game";
    NAME.OPTIONS = 'Options';
    NAME.HELP = 'Help';
    NAME.MAP_EDITOR = 'Map Editor';
    NAME.CONTINUE_GAME = "Continue Game";
    NAME.EXIT = "Exit";
    NAME.LOADING = "Loading";
    NAME.MAIN_SCREEN = "Main Screen";
    NAME.RETURN_TO_MAIN_SCREEN = "Return to Main Screen";
    NAME.GAME = "Game";
    NAME.LEVEL = "Level";
    NAME.MAP = "Map";
    NAME.LOADING_RESOURCE = "Loading Resources";
    NAME.LOADING_MODS = "Loading Mods";
    NAME.WRONG_COMMAND = 'Wrong Command';
    NAME.DIRT_TYPE1 = 'Dirt Type 1';
    NAME.GRASS_TYPE1 = 'Grass Type 1';
    NAME.GRASS_TYPE2 = 'Grass Type 2'
    NAME.GRASS_TYPE3 = 'Grass Type 3'
    NAME.GROUND_TYPE1 = 'Ground Type 1';
    NAME.LAVA_TYPE1 = 'Lava Type 1';
    NAME.WATER_TYPE1 = 'Water Type 1';
    NAME.WATER_TYPE2 = 'Water Type 2';
    NAME.WIDTH = 'Width';
    NAME.HEIGHT = 'Height';
    NAME.NAME_THE_MAP = 'Name the map';
    NAME.MAP_SAVE = 'Map saved';
    NAME.FAIL_TO_SAVE_MAP = 'Fail to save map';
    NAME.GUI_FAILED_TO_LOAD_ELEMENT_NAMED = 'GUI failed to load element named';
    NAME.GUI_FAILED_TO_LOAD_FUNCTION_NAMED = 'GUI failed to load function named';
    NAME.GUI_FAILED_TO_CALL_FUNCTION_NAMED = 'GUI failed to call function named';
    NAME.GUI_FAILED_TO_DELETE_ELEMERNT_NAMED = 'GUI failed to delete element named';
    NAME.GUI_FAILED_TO_DELETE_FUNCTION_NAMED = 'GUI failed to delete function named';
    NAME.GUI_ERROR_UNKNOWN_POSITION = 'GUI error, unknown position';

};
