;moudle = async (COM) => {

const app = await require('var/app');

COM.help = (n = 1) => {
    return n == 1 ? `
欢迎使用控制台模块（使用"h"键关闭输出消隐）

使用/help <n> 输出帮助手册的第<n>页

在控制台中，你可以操控游戏的方方面面，
通过 "CONST.<参数名> = <参数值>" 指令，你可以修改游戏的静态设置参数
通过 "app.<参数名> = <参数值>" 指令，你可以修改游戏的运行时数据

目录
* P2指令列表
`:n==2?`
/echo <args> 输出对应内容
/clear 清除控制台
/help <n> 输出帮助手册的第<n>页
`:'';
};

COM.echo = (...args) => {
    for (let arg of args) {
        app.console.print(arg, 5000, "#ff0");
    }
};

COM.clear = () => {
    app.console.logs = app.console.logs.filter((elem) => elem.time === Infinity);
}

};
